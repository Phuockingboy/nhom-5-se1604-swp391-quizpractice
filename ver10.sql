
GO
ALTER DATABASE [ProjectSWP_KS04] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ProjectSWP_KS04] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProjectSWP_KS04] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProjectSWP_KS04] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ProjectSWP_KS04] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProjectSWP_KS04] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ProjectSWP_KS04] SET  MULTI_USER 
GO
ALTER DATABASE [ProjectSWP_KS04] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProjectSWP_KS04] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProjectSWP_KS04] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProjectSWP_KS04] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ProjectSWP_KS04] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ProjectSWP_KS04] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [ProjectSWP_KS04] SET QUERY_STORE = OFF
GO
USE [ProjectSWP_KS04]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[categoryid] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [nvarchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[categoryid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[category_Quiz]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category_Quiz](
	[quizid] [int] NOT NULL,
	[categoryid] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[quizid] ASC,
	[categoryid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[classid] [int] IDENTITY(1,1) NOT NULL,
	[createid] [int] NOT NULL,
	[classname] [nvarchar](200) NOT NULL,
	[DateCreated] [date] NOT NULL,
	[code] [varchar](30) NOT NULL,
	[allowOthercreateQuiz] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[classid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class_User]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class_User](
	[classid] [int] NOT NULL,
	[userid] [int] NOT NULL,
	[dateJoin] [date] NOT NULL,
	[isAdmin] [bit] NOT NULL,
	[joinBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[classid] ASC,
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClassLog]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassLog](
	[logid] [bigint] IDENTITY(1,1) NOT NULL,
	[classid] [int] NOT NULL,
	[datelog] [date] NOT NULL,
	[Content] [nvarchar](1000) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[logid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Option]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Option](
	[quizid] [int] NOT NULL,
	[questionid] [int] NOT NULL,
	[optionid] [int] NOT NULL,
	[option_content] [nvarchar](2000) NOT NULL,
	[right_option] [bit] NOT NULL,
 CONSTRAINT [PK__Option__41E528F5AC451FA9] PRIMARY KEY CLUSTERED 
(
	[quizid] ASC,
	[questionid] ASC,
	[optionid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Option_test]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Option_test](
	[testid] [int] NOT NULL,
	[questionid] [int] NOT NULL,
	[optionid] [int] NOT NULL,
	[option_content] [nvarchar](2000) NOT NULL,
	[right_option] [bit] NOT NULL,
	[option_status] [bit] NULL,
 CONSTRAINT [PK__Option_t__2C8B9F003FF15BF2] PRIMARY KEY CLUSTERED 
(
	[testid] ASC,
	[questionid] ASC,
	[optionid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[quizid] [int] NOT NULL,
	[questionid] [int] NOT NULL,
	[question_title] [nvarchar](1000) NOT NULL,
	[Intruction] [nvarchar](2000) NULL,
	[randomOption] [bit] NOT NULL,
 CONSTRAINT [PK__Question__79D86A03F3B97BCE] PRIMARY KEY CLUSTERED 
(
	[quizid] ASC,
	[questionid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question_test]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question_test](
	[questionid] [int] NOT NULL,
	[testid] [int] NOT NULL,
	[question_title] [nvarchar](1000) NOT NULL,
	[Intruction] [nvarchar](2000) NULL,
	[question_status] [bit] NULL,
	[numberOfRightAnswer] [int] NULL,
	[done] [bit] NOT NULL,
 CONSTRAINT [PK__Question__14B6DDF6BCEE48D0] PRIMARY KEY CLUSTERED 
(
	[testid] ASC,
	[questionid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz](
	[quizid] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[title] [nvarchar](1000) NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[Date_Created] [date] NOT NULL,
	[Last_update] [date] NULL,
	[classid] [int] NULL,
 CONSTRAINT [PK__Quiz__CFF448152B26B3E7] PRIMARY KEY CLUSTERED 
(
	[quizid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rate]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rate](
	[rateid] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[quizid] [int] NOT NULL,
	[rate] [int] NOT NULL,
	[feedback] [nvarchar](1000) NULL,
 CONSTRAINT [PK__Rate__5704EE3C553055E7] PRIMARY KEY CLUSTERED 
(
	[rateid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Test]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[testid] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[quizid] [int] NOT NULL,
	[title] [nvarchar](1000) NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[Date_Created] [date] NOT NULL,
	[Last_update] [date] NULL,
	[NumberOfQuestion] [int] NULL,
	[Mark] [float] NULL,
	[time] [int] NULL,
	[correctAnswer] [int] NULL,
 CONSTRAINT [PK__Test__A29AFFE0B39EC991] PRIMARY KEY CLUSTERED 
(
	[testid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[username] [nchar](50) NOT NULL,
	[password] [nchar](30) NOT NULL,
	[email] [nchar](50) NOT NULL,
	[aboutme] [nvarchar](1000) NULL,
	[phone] [nchar](20) NULL,
	[avatar] [nchar](1000) NULL,
	[role_id] [int] NOT NULL,
 CONSTRAINT [PK__User__CBA1B25752AD2A82] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[View]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[View](
	[userid] [int] NOT NULL,
	[quizid] [int] NOT NULL,
	[date] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[userid] ASC,
	[quizid] ASC,
	[date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Visitor]    Script Date: 7/21/2022 9:22:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visitor](
	[Time] [date] NOT NULL,
	[numberOfView] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (1, N'Language')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (2, N'Academic')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (3, N'Mathematic')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (4, N'Philosophy')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (5, N'History')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (6, N'Physics')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (7, N'Chemistry')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (8, N'Literary')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (9, N'Computer science')
INSERT [dbo].[Category] ([categoryid], [category_name]) VALUES (10, N'Art')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (1, 4)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (5, 1)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (6, 1)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (7, 1)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (9, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (10, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (11, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (12, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (12, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (13, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (15, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (16, 1)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (17, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (17, 9)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (18, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (18, 9)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (22, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (23, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (24, 1)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (25, 1)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (25, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (27, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (27, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (28, 2)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (28, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (36, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (37, 3)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (38, 1)
INSERT [dbo].[category_Quiz] ([quizid], [categoryid]) VALUES (39, 3)
GO
SET IDENTITY_INSERT [dbo].[Class] ON 

INSERT [dbo].[Class] ([classid], [createid], [classname], [DateCreated], [code], [allowOthercreateQuiz]) VALUES (1, 1, N'KS04', CAST(N'2022-11-12' AS Date), N'0111122022th', 0)
INSERT [dbo].[Class] ([classid], [createid], [classname], [DateCreated], [code], [allowOthercreateQuiz]) VALUES (2, 1, N'KS04_2', CAST(N'2022-07-19' AS Date), N'123456789010', 0)
INSERT [dbo].[Class] ([classid], [createid], [classname], [DateCreated], [code], [allowOthercreateQuiz]) VALUES (3, 2, N'T Class', CAST(N'2022-07-19' AS Date), N'Thuong01111111111', 1)
SET IDENTITY_INSERT [dbo].[Class] OFF
GO
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (1, 1, CAST(N'2022-07-18' AS Date), 1, 1)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (1, 2, CAST(N'2022-07-20' AS Date), 1, 0)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (1, 3, CAST(N'2022-07-18' AS Date), 0, 2)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (1, 8, CAST(N'2022-07-19' AS Date), 1, 0)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (2, 1, CAST(N'2022-07-18' AS Date), 1, 1)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (2, 2, CAST(N'2022-07-19' AS Date), 0, 0)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (3, 2, CAST(N'2022-07-20' AS Date), 1, 2)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (3, 8, CAST(N'2022-07-20' AS Date), 0, 2)
INSERT [dbo].[Class_User] ([classid], [userid], [dateJoin], [isAdmin], [joinBy]) VALUES (3, 17, CAST(N'2022-07-20' AS Date), 0, 2)
GO
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 1, 1, N'180', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 1, 2, N'200', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 1, 3, N'360', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 1, 4, N'330', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 2, 1, N'1', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 2, 2, N'0', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 2, 3, N'countless', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 3, 1, N'on', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 3, 2, N'near', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 3, 3, N'along', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 3, 4, N'at', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 4, 1, N'of', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 4, 2, N'for', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 4, 3, N'due to', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (1, 4, 4, N'because of', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (5, 1, 1, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (5, 1, 2, N'e', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (5, 1, 3, N'e', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (5, 2, 1, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (5, 2, 2, N't', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (5, 2, 3, N'u', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (6, 1, 1, N'Æ°', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (6, 1, 2, N'Æ°', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (6, 1, 3, N'Æ°', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (7, 1, 1, N'Æ°', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (7, 1, 2, N'Æ°', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (7, 1, 3, N'Æ°', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (8, 1, 1, N'3', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (8, 1, 2, N'3', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (9, 1, 1, N'1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (9, 1, 2, N'2', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (9, 1, 3, N'3', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (9, 2, 1, N'1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (9, 2, 2, N'3', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (9, 2, 3, N'4', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (10, 1, 1, N'1', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (10, 1, 2, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (10, 1, 3, N'3', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (10, 2, 1, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (10, 2, 2, N'7', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (10, 2, 3, N'9', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (11, 1, 1, N'1', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (11, 1, 2, N'3', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (11, 1, 3, N'5', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (11, 1, 4, N'q', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (11, 2, 1, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (11, 2, 2, N'3', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (11, 2, 3, N'4', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (12, 1, 1, N'1', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (12, 1, 2, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (12, 1, 3, N'3', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (12, 2, 1, N'11', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (12, 2, 2, N'12', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (12, 2, 3, N'13', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 1, 1, N'18', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 1, 2, N'7', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 1, 3, N'11', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 1, 4, N'25', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 1, 5, N'12', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 2, 1, N'26', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 2, 2, N'36', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 2, 3, N'24', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 2, 4, N'74', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 3, 1, N'400', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 3, 2, N'402', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 3, 3, N'404', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 3, 4, N'670', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 3, 5, N'938', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 4, 1, N'4000 + 500 + 60 + 7', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 4, 2, N'4000 + 50 + 6 + 7', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 4, 3, N'4000 + 506 + 67', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (13, 4, 4, N'400 + 500 + 60 + 7', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (15, 1, 1, N'in', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (15, 1, 2, N'beside', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (15, 1, 3, N'am', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (15, 1, 4, N'to', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 1, 1, N'Look angrily', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 1, 2, N'Major share', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 1, 3, N'Minor share', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 1, 4, N'Heart of the prey', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 2, 1, N'Resemble', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 2, 2, N'Easy', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 2, 3, N'Impossible', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 2, 4, N'For the end', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 3, 1, N'Warmly', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 3, 2, N'Cold-blooded', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 3, 3, N'Quietly', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 3, 4, N'Coldly', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 4, 1, N'Feel greatly', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 4, 2, N'Standard', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 4, 3, N'Extinguish', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (16, 4, 4, N'Below standard', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 1, 1, N's2 = s1;', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 1, 2, N'strcpy(s2,s1);', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 1, 3, N's2 == s1;', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 1, 4, N'all three are wrong', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 2, 1, N'Positive number', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 2, 2, N'Even number', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 2, 3, N'Odd number', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 2, 4, N'Prime number', 0)
GO
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 3, 1, N'8', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 3, 2, N'9', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 3, 3, N'10', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 3, 4, N'11', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 4, 1, N'Zero', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 4, 2, N'Base - 1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 4, 3, N'Base 1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 4, 4, N'none of the above', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 5, 1, N'to write output to the screen', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 5, 2, N'the program entry point', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 5, 3, N'Both correct', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (17, 5, 4, N'Both wrong', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 1, 1, N's2 = s1;', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 1, 2, N'strcpy(s2,s1);', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 1, 3, N's2 == s1;', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 1, 4, N'all three are wrong', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 2, 1, N'Positive number', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 2, 2, N'Even number', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 2, 3, N'Odd number', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 2, 4, N'Prime number', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 3, 1, N'8', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 3, 2, N'9', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 3, 3, N'10', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 3, 4, N'11', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 4, 1, N'Zero', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 4, 2, N'Base - 1', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 4, 3, N'Base 1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 4, 4, N'none of the above', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 5, 1, N'to write output to the screen', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 5, 2, N'the program entry point', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 5, 3, N'Both correct', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (18, 5, 4, N'Both wrong', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (22, 1, 1, N'1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (22, 1, 2, N'12', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (22, 1, 3, N'11', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (23, 1, 1, N'1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (23, 1, 2, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 1, 1, N'gave him some lip', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 1, 2, N'gave him a buzz', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 1, 3, N'gave him the air', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 1, 4, N'gave him a hard time', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 1, 5, N'gave him a five', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 2, 1, N'throw into the bargain', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 2, 2, N'throw in the towel', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 2, 3, N'throw into sharp relief', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 2, 4, N'throw in at the deep end', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 2, 5, N'throw down the gauntlet', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 3, 1, N'Stay abreast', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 3, 2, N'Stay after', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 3, 3, N'Stay back', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 3, 4, N'Stay down', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (24, 3, 5, N'Stay over', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 1, 1, N'gets under my skin', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 1, 2, N'gets up a head of steam', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 1, 3, N'gets up on my hind legs', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 1, 4, N'gets up an appetite', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 1, 5, N'gets the worst of it', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 2, 1, N'call on the carpet', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 2, 2, N'call it a day', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 2, 3, N'call his bluff', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 2, 4, N'call the shots', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 2, 5, N'call off the dogs', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 3, 1, N'has the ear of', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 3, 2, N'has the best of', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 3, 3, N'has the blues of', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 3, 4, N'has the edge on', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 3, 5, N'has the guts over', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 4, 1, N'on the dot', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 4, 2, N'on the cuff', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 4, 3, N'on the carpet', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 4, 4, N'on the fence', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (25, 4, 5, N'on the double', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (27, 1, 1, N'1', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (27, 1, 2, N'11', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (27, 1, 3, N'12', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (27, 1, 4, N'13', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 1, 1, N'11111', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 1, 2, N'12344', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 1, 3, N'123456789', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 1, 4, N'234679876543234567890', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 2, 1, N'1234567', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 2, 2, N'123456', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 2, 3, N'dfrgthyjkl', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 3, 1, N'nhịn', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 3, 2, N'ăn bún', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 3, 3, N'ăn cơm', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 4, 1, N'ádfghj', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 4, 2, N'ưertyuio', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 4, 3, N'qưertyui', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (28, 4, 4, N'wertyuio;', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 1, 1, N'1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 1, 2, N'hi', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 1, 3, N'bye', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 1, 4, N'alo', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 2, 1, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 2, 2, N'3', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 2, 3, N'4', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (36, 2, 4, N'5', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 1, 1, N'12', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 1, 2, N'13', 0)
GO
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 1, 3, N'14', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 1, 4, N'20', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 2, 1, N'3', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 2, 2, N'4', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 2, 3, N'5', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 2, 4, N'6', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 3, 1, N'1', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 3, 2, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 3, 3, N'4', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (37, 3, 4, N'5', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (38, 1, 1, N'1', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (38, 1, 2, N'2', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (38, 1, 3, N'3', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (38, 1, 4, N'4', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (39, 1, 1, N'as', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (39, 1, 2, N'bc', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (39, 1, 3, N'rc', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (39, 2, 1, N'4', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (39, 2, 2, N'4', 1)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (39, 2, 3, N'6', 0)
INSERT [dbo].[Option] ([quizid], [questionid], [optionid], [option_content], [right_option]) VALUES (39, 2, 4, N'5', 0)
GO
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 1, 1, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 1, 2, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 1, 3, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 1, 4, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 2, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 2, 2, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 2, 3, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 3, 1, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 3, 2, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 3, 3, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (4, 3, 4, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 1, 1, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 1, 2, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 1, 3, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 1, 4, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 2, 1, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 2, 2, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 2, 3, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 2, 4, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 3, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 3, 2, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (5, 3, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 1, 1, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 1, 2, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 1, 3, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 1, 4, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 2, 1, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 2, 2, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 2, 3, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 2, 4, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 3, 1, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 3, 2, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (6, 3, 3, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 1, 1, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 1, 2, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 1, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 1, 4, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 2, 1, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 2, 2, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 2, 3, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (7, 2, 4, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 1, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 1, 2, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 1, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 2, 1, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 2, 2, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 2, 3, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 2, 4, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 3, 1, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 3, 2, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 3, 3, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (8, 3, 4, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 1, 1, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 1, 2, N'along', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 1, 3, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 1, 4, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 2, 1, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 2, 2, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 2, 3, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 3, 1, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 3, 2, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 3, 3, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (9, 3, 4, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 1, 1, N'200', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 1, 2, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 1, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 1, 4, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 2, 1, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 2, 2, N'on', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 2, 3, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 2, 4, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 3, 1, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 3, 2, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 3, 3, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (10, 3, 4, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 1, 1, N'on', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 1, 2, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 1, 3, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 1, 4, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 2, 1, N'for', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 2, 2, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 2, 3, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 2, 4, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 3, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 3, 2, N'countless', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 3, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 4, 1, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 4, 2, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 4, 3, N'180', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (11, 4, 4, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 1, 1, N'200', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 1, 2, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 1, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 1, 4, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 2, 1, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 2, 2, N'near', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 2, 3, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 2, 4, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 3, 1, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 3, 2, N'of', 1, 1)
GO
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 3, 3, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 3, 4, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 4, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 4, 2, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (12, 4, 3, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 1, 1, N'360', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 1, 2, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 1, 3, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 1, 4, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 2, 1, N'0', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 2, 2, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 2, 3, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 3, 1, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 3, 2, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 3, 3, N'on', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 3, 4, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 4, 1, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 4, 2, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 4, 3, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (13, 4, 4, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 1, 1, N'at', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 1, 2, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 1, 3, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 1, 4, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 2, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 2, 2, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 2, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 3, 1, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 3, 2, N'for', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 3, 3, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 3, 4, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 4, 1, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 4, 2, N'200', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 4, 3, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (14, 4, 4, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 1, 1, N'for', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 1, 2, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 1, 3, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 1, 4, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 2, 1, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 2, 2, N'180', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 2, 3, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 2, 4, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 3, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 3, 2, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 3, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 4, 1, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 4, 2, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 4, 3, N'at', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (15, 4, 4, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 1, 1, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 1, 2, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 1, 3, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 1, 4, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 2, 1, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 2, 2, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 2, 3, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 2, 4, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 3, 1, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 3, 2, N'countless', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 3, 3, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 4, 1, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 4, 2, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 4, 3, N'at', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (16, 4, 4, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 1, 1, N'near', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 1, 2, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 1, 3, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 1, 4, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 2, 1, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 2, 2, N'360', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 2, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 2, 4, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 3, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 3, 2, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (17, 3, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 1, 1, N'because of', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 1, 2, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 1, 3, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 1, 4, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 2, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 2, 2, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 2, 3, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 3, 1, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 3, 2, N'180', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 3, 3, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (18, 3, 4, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 1, 1, N'200', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 1, 2, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 1, 3, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 1, 4, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 2, 1, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 2, 2, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 2, 3, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 2, 4, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 3, 1, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 3, 2, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 3, 3, N'due to', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (19, 3, 4, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 1, 1, N'360', 0, 1)
GO
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 1, 2, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 1, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 1, 4, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 2, 1, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 2, 2, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 2, 3, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (20, 2, 4, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (21, 1, 1, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (21, 1, 2, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (21, 1, 3, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (21, 1, 4, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 1, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 1, 2, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 1, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 2, 1, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 2, 2, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 2, 3, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 2, 4, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 3, 1, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 3, 2, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 3, 3, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (22, 3, 4, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 1, 1, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 1, 2, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 1, 3, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 1, 4, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 2, 1, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 2, 2, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 2, 3, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 2, 4, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 3, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 3, 2, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (23, 3, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 1, 1, N'at', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 1, 2, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 1, 3, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 1, 4, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 2, 1, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 2, 2, N'360', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 2, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 2, 4, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 3, 1, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 3, 2, N'0', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (24, 3, 3, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 1, 1, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 1, 2, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 1, 3, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 1, 4, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 2, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 2, 2, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 2, 3, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 3, 1, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 3, 2, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 3, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (25, 3, 4, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 1, 1, N'on', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 1, 2, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 1, 3, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 1, 4, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 2, 1, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 2, 2, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 2, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 2, 4, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 3, 1, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 3, 2, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 3, 3, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 3, 4, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 4, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 4, 2, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (26, 4, 3, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (27, 1, 1, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (27, 1, 2, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (27, 1, 3, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (27, 1, 4, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (27, 2, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (27, 2, 2, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (27, 2, 3, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 1, 1, N'along', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 1, 2, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 1, 3, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 1, 4, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 2, 1, N'180', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 2, 2, N'360', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 2, 3, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 2, 4, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 3, 1, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 3, 2, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (28, 3, 3, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 1, 1, N'123456789', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 1, 2, N'12344', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 1, 3, N'11111', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 1, 4, N'234679876543234567890', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 2, 1, N'nhịn', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 2, 2, N'ăn bún', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 2, 3, N'ăn cơm', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 3, 1, N'ưertyuio', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 3, 2, N'ádfghj', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 3, 3, N'wertyuio;', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 3, 4, N'qưertyui', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 4, 1, N'123456', 0, 0)
GO
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 4, 2, N'dfrgthyjkl', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (29, 4, 3, N'1234567', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (30, 1, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (30, 1, 2, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (30, 1, 3, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (31, 1, 1, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (31, 1, 2, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (31, 1, 3, N'at', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (31, 1, 4, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 1, 1, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 1, 2, N'0', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 1, 3, N'countless', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 2, 1, N'along', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 2, 2, N'at', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 2, 3, N'on', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 2, 4, N'near', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 3, 1, N'200', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 3, 2, N'330', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 3, 3, N'180', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 3, 4, N'360', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 4, 1, N'for', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 4, 2, N'of', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 4, 3, N'due to', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (32, 4, 4, N'because of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (33, 1, 1, N'12', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (33, 1, 2, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (33, 1, 3, N'11', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (33, 1, 4, N'13', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (34, 1, 1, N'13', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (34, 1, 2, N'12', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (34, 1, 3, N'11', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (34, 1, 4, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (35, 1, 1, N'1', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (35, 1, 2, N'13', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (35, 1, 3, N'12', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (35, 1, 4, N'11', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (36, 1, 1, N'1', 1, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (36, 1, 2, N'11', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (36, 1, 3, N'12', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (36, 1, 4, N'13', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (37, 1, 1, N'on the carpet', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (37, 1, 2, N'on the double', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (37, 1, 3, N'on the cuff', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (37, 1, 4, N'on the dot', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (37, 1, 5, N'on the fence', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (38, 1, 1, N'on the fence', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (38, 1, 2, N'on the carpet', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (38, 1, 3, N'on the double', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (38, 1, 4, N'on the cuff', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (38, 1, 5, N'on the dot', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (39, 1, 1, N'has the blues of', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (39, 1, 2, N'has the best of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (39, 1, 3, N'has the ear of', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (39, 1, 4, N'has the guts over', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (39, 1, 5, N'has the edge on', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (40, 1, 1, N'on the carpet', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (40, 1, 2, N'on the fence', 0, 1)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (40, 1, 3, N'on the double', 1, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (40, 1, 4, N'on the dot', 0, 0)
INSERT [dbo].[Option_test] ([testid], [questionid], [optionid], [option_content], [right_option], [option_status]) VALUES (40, 1, 5, N'on the cuff', 0, 0)
GO
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (1, 1, N'What is the sum of the 3 angles of a triangle?', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (1, 2, N'How many planes are formed from a point and a line that does not contain that point?', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (1, 3, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (1, 4, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (5, 1, N'thuong', N'thuong', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (5, 2, N'thh', N'r', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (6, 1, N'Æ°', N'Æ°', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (7, 1, N'thuong ,  thuong', N'Æ°', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (8, 1, N'1+1= ?', N'3', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (9, 1, N'2=?', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (9, 2, N'3=?', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (10, 1, N'1=?', N'ee', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (10, 2, N'3^2=?', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (11, 1, N'1', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (11, 2, N'1=?', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (12, 1, N'1', N't', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (12, 2, N'Raina is making an apple pie. She needs 18 apples to make it. She has 7 apples. How many more apples does she need to make it?', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (13, 1, N'Raina is making an apple pie. She needs 18 apples to make it. She has 7 apples. How many more apples does she need to make it?', N'Riana already has 7 apples and in total she needs 18 apples. So to know the number of more apples she needs, we have to subtract the total number of apples from number of apple she already has to get the answer. 18 - 7 = 11', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (13, 2, N'What number should replace the question mark (?) 50 - ? = 24', N'This is right answer. To get the number, we have to subtract 24 from 50. 50 - 24 = 26', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (13, 3, N'Kushal is going to meet his parents. For that he has to travel 670 km. He drives 268 km and stop to take rest. How many more km must Kushal travel to meet his parents?', N'This is a right answer. To find the distance that Kushal has to travel more, we have to subtract the distance that he has travelled from the total distance that he has to travel 650 - 268 = 402', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (13, 4, N'Which has the same value as the number 4567?', N'This is the right answer. 4000 + 500 + 60 + 7 is equal to 4567.', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (15, 1, N'Which is NOT a preposition?', N'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (16, 1, N'Directions: Choose the option that correctly defines the meaning of the given idiom/phrase.', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (16, 2, N'Choose the option that correctly defines the meaning of the given idiom/phrase.', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (16, 3, N'Choose the option that correctly defines the meaning of the given idiom/phrase.', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (16, 4, N'Choose the option that correctly defines the meaning of the given idiom/phrase.', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (17, 1, N'To copy a string into another, given: char s1[30] = \"xyz\", s2[20];', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (17, 2, N'If LSB of a binary number is \"0\" then it is:', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (17, 3, N'What is the output of the following piece of code?  int x = 3, y = 5, z; z = x + ++y;  printf(“%d”,z);', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (17, 4, N'The highest digit in any number system equals:', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (17, 5, N'The main() function is:', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (18, 1, N'To copy a string into another, given: char s1[30] = \"xyz\", s2[20];', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (18, 2, N'If LSB of a binary number is \"0\" then it is:', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (18, 3, N'What is the output of the following piece of code?  int x = 3, y = 5, z; z = x + ++y;  printf(“%d”,z);', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (18, 4, N'The highest digit in any number system equals:', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (18, 5, N'The main() function is:', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (22, 1, N'1=', N'"', 0)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (23, 1, N'1=', N'"', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (24, 1, N'Fill in the blank with the suitable idiom/phrase.\nThe teacher ___________ for not showing up at the extra classes for the weak students of the class.', N'This is the correct option. To ''gave him a hard time'' implies to make someone feel bad for a mistake. The teacher gave him a hard time for not showing up at the extra classes for the weak students of the class."', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (24, 2, N'Fill in the blank with the suitable idiom/phrase.\nHe is a hard worker. He is not going to ________ that easily.', N'This is the correct option. To ''throw in the towel'' implies to quit or to surrender. He is a hard worker. He is not going to throw in the towel that easily."', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (24, 3, N'Fill in the blank with the suitable idiom/phrase.\n___________ from the highway. It is dangerous as so much traffic passes rough it at high speed!', N'This is the correct option. To ''stay back'' implies to keep distance from something that is harmful. Stay back from the highway. It is dangerous as so much traffic pass through it at high speed!"', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (25, 1, N'Fill in the blank with the suitable idiom/phrase.\\\\nHis continuous pestering for money really __________.', N'To ''gets under my skin'' here implies to bother or irritate me. His continuous pestering for money really gets under my skin.\\\"', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (25, 2, N'Fill in the blank with the suitable idiom/phrase.\\\\nThe opposition should _________ and give some time to the new Chief Minister to act.', N'To ''call off the dogs'' implies to stop criticizing someone over something. The opposition should call off the dogs and give some time to the new Chief Minister to act.\\\"', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (25, 3, N'Fill in the blank with the suitable idiom/phrase.\\\\nThe new Mercedes Benz _________ other similar models - its larger and quicker!', N'To ''have the edge on'' someone implies to be slightly better than something else. The new Mercedes Benz has the edge on other similar models - its larger and quicker!\\\"', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (25, 4, N'Fill in the blank with the suitable idiom/phrase.\\\\nThe word was sent out that the King wanted to see his defence minister ____________.', N'''On the double'' implies very fast or twice as fast. The word was sent out that the King wanted to see his defence minister on the double.\\\"', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (27, 1, N'11=?', N'11=10+1', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (28, 1, N'ngu', N'1111', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (28, 2, N'123456789098765432', N'ádfghjkllkjhgfds', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (28, 3, N'nay mùi ăn gì', N'', 0)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (28, 4, N'12345678', N'ádfghj', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (36, 1, N'Hello', N'Greetting', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (36, 2, N'what diference from others', N'', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (37, 1, N'12+8=?', N'20', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (37, 2, N'30/10', N'3', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (37, 3, N'12-7=?', N'5', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (38, 1, N'1', N'1', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (39, 1, N'1', N'1', 1)
INSERT [dbo].[Question] ([quizid], [questionid], [question_title], [Intruction], [randomOption]) VALUES (39, 2, N'4', N'4', 1)
GO
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 4, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 4, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 4, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 5, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 5, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 5, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 6, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 6, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 6, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 7, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 7, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 8, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 8, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 8, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 9, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 9, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 9, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 10, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 10, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 10, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 11, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 11, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 11, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 11, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 12, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 12, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 12, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 12, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 13, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 13, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 13, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 13, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 14, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 14, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 14, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 14, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 15, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 15, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 15, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 15, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 16, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 16, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 16, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 16, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 17, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 17, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 17, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 18, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 18, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 18, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 19, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 19, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 19, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 20, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 20, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 21, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 22, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 22, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 22, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 23, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 23, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 23, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 24, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 24, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 24, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 25, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 25, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 25, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 26, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 26, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 26, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 26, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 27, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 27, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 28, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 28, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 28, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 29, N'ngu', N'1111', 0, 2, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 29, N'nay mùi ăn gì', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 29, N'12345678', N'ádfghj', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 29, N'123456789098765432', N'ádfghjkllkjhgfds', 0, 0, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 30, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 31, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 32, N'How many planes are formed from a point and a line that does not contain that point?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (2, 32, N'Turtles can sleep suspended _________________ the surface of the water.', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (3, 32, N'What is the sum of the 3 angles of a triangle?', N'', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (4, 32, N'Should compensation be paid to victimes _________________ nuclear tests?', N'A', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 33, N'11=?', N'11=10+1', 0, 2, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 34, N'11=?', N'11=10+1', 0, 2, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 35, N'11=?', N'11=10+1', 0, 2, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 36, N'11=?', N'11=10+1', 0, 2, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 37, N'Fill in the blank with the suitable idiom/phrase.\\\\nThe word was sent out that the King wanted to see his defence minister ____________.', N'''On the double'' implies very fast or twice as fast. The word was sent out that the King wanted to see his defence minister on the double.\\\"', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 38, N'Fill in the blank with the suitable idiom/phrase.\\\\nThe word was sent out that the King wanted to see his defence minister ____________.', N'''On the double'' implies very fast or twice as fast. The word was sent out that the King wanted to see his defence minister on the double.\\\"', 0, 1, 0)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 39, N'Fill in the blank with the suitable idiom/phrase.\\\\nThe new Mercedes Benz _________ other similar models - its larger and quicker!', N'To ''have the edge on'' someone implies to be slightly better than something else. The new Mercedes Benz has the edge on other similar models - its larger and quicker!\\\"', 0, 1, 1)
INSERT [dbo].[Question_test] ([questionid], [testid], [question_title], [Intruction], [question_status], [numberOfRightAnswer], [done]) VALUES (1, 40, N'Fill in the blank with the suitable idiom/phrase.\\\\nThe word was sent out that the King wanted to see his defence minister ____________.', N'''On the double'' implies very fast or twice as fast. The word was sent out that the King wanted to see his defence minister on the double.\\\"', 0, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Quiz] ON 

INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (1, 2, N'Test Create Quiz', N'16/2', CAST(N'2022-06-12' AS Date), CAST(N'2022-06-30' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (5, 2, N'Test Create Quiz 2', N'12', CAST(N'2022-06-13' AS Date), CAST(N'2022-07-05' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (6, 2, N'Test Create Quiz', N'Æ°', CAST(N'2022-06-13' AS Date), CAST(N'2022-07-05' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (7, 2, N'Test Create Quiz', N'thÆ°Æ¡ng', CAST(N'2022-06-13' AS Date), CAST(N'2022-07-05' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (8, 2, N'Test Create Quiz', N'www thÆ°Æ¡ng', CAST(N'2022-06-13' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (9, 2, N'thương', N'test14/6', CAST(N'2022-06-14' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (10, 2, N'Tét ite1', N'14/6 part 1', CAST(N'2022-06-14' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (11, 2, N'thuong', N'1', CAST(N'2022-06-14' AS Date), CAST(N'2022-06-23' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (12, 2, N'Tét ite2', N'part', CAST(N'2022-06-14' AS Date), CAST(N'2022-06-18' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (13, 3, N'Mathematic for class 3', N'', CAST(N'2022-06-16' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (15, 2, N'English Grammar', N'English Language Arts Grammar Online Quiz
Welcome You', CAST(N'2022-06-17' AS Date), CAST(N'2022-06-20' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (16, 3, N'Idioms & Phrases', N'Practice the Idioms & Phrases', CAST(N'2022-06-20' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (17, 3, N'C Programming Language', N'C Programming Language Quiz -1 ', CAST(N'2022-06-20' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (18, 3, N'C Programming Language', N'C Programming Language Quiz -1 ', CAST(N'2022-06-20' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (22, 2, N'mathematic', N'qw', CAST(N'2022-06-27' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (23, 2, N'mathematic', N'', CAST(N'2022-06-27' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (24, 2, N'thươngdt he150682', N'28/06 test', CAST(N'2022-06-28' AS Date), NULL, 1)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (25, 2, N'Idioms and Phrases', N'Idioms and Phrases', CAST(N'2022-06-28' AS Date), CAST(N'2022-06-30' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (27, 2, N'tesst 30/6', N'tesst add random option for question', CAST(N'2022-06-30' AS Date), CAST(N'2022-06-30' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (28, 2, N'ngày 13/7', N'hôm nay là 13/7', CAST(N'2022-07-13' AS Date), NULL, 1)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (36, 2, N'Test create quiz 19/7', N'ite4', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (37, 2, N'Test 19/7 last', N'19/7', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-20' AS Date), NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (38, 2, N'1', N'1', CAST(N'2022-07-19' AS Date), NULL, NULL)
INSERT [dbo].[Quiz] ([quizid], [userid], [title], [Description], [Date_Created], [Last_update], [classid]) VALUES (39, 2, N'20/7', N'thương', CAST(N'2022-07-20' AS Date), NULL, 1)
SET IDENTITY_INSERT [dbo].[Quiz] OFF
GO
SET IDENTITY_INSERT [dbo].[Rate] ON 

INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (1, 1, 5, 3, N'aaaaaaaaaaaaaaaaa')
INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (2, 1, 6, 4, N'ccccccccc')
INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (3, 1, 7, 2, N'áđá ')
INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (4, 7, 5, 4, N'xxxxxx')
INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (6, 3, 5, 4, N'asdfg ')
INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (9, 5, 5, 2, N'fffffff')
INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (10, 6, 5, 4, N'quiz hay')
INSERT [dbo].[Rate] ([rateid], [userid], [quizid], [rate], [feedback]) VALUES (21, 2, 18, 4, N'Good                         ')
SET IDENTITY_INSERT [dbo].[Rate] OFF
GO
SET IDENTITY_INSERT [dbo].[Test] ON 

INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (2, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 100, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (3, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 20, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (4, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 20, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (5, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 2, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (6, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 20, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (7, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 2, NULL, 3, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (8, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 3, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (9, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 3, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (10, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 20, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (11, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 4, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (12, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 5, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (13, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 5, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (14, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 4, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (15, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 4, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (16, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 4, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (17, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 60, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (18, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (19, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 3, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (20, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 2, NULL, 2, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (21, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-12' AS Date), CAST(N'2022-07-12' AS Date), 1, NULL, 6, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (22, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 3, NULL, 4, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (23, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 3, NULL, 4, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (24, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 3, NULL, 5, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (25, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 3, NULL, 4, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (26, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 4, NULL, 3, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (27, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 2, NULL, 30, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (28, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 3, NULL, 20, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (29, 2, 28, N'ngày 13/7', N'hôm nay là 13/7', CAST(N'2022-07-13' AS Date), CAST(N'2022-07-13' AS Date), 4, NULL, 6, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (30, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-16' AS Date), CAST(N'2022-07-16' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (31, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-16' AS Date), CAST(N'2022-07-16' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (32, 2, 1, N'Test Create Quiz', N'16/2', CAST(N'2022-07-18' AS Date), CAST(N'2022-07-18' AS Date), 4, NULL, 3, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (33, 2, 27, N'tesst 30/6', N'tesst add random option for question', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 7, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (34, 2, 27, N'tesst 30/6', N'tesst add random option for question', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (35, 2, 27, N'tesst 30/6', N'tesst add random option for question', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (36, 2, 27, N'tesst 30/6', N'tesst add random option for question', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (37, 2, 25, N'Idioms and Phrases', N'Idioms and Phrases', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (38, 2, 25, N'Idioms and Phrases', N'Idioms and Phrases', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (39, 2, 25, N'Idioms and Phrases', N'Idioms and Phrases', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 1, NULL)
INSERT [dbo].[Test] ([testid], [userid], [quizid], [title], [Description], [Date_Created], [Last_update], [NumberOfQuestion], [Mark], [time], [correctAnswer]) VALUES (40, 2, 25, N'Idioms and Phrases', N'Idioms and Phrases', CAST(N'2022-07-19' AS Date), CAST(N'2022-07-19' AS Date), 1, NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[Test] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (1, N'User One                                          ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'abcgfhd@gmail.com                                 ', N'Love sleeping', N'0111222333          ', NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (2, N'Thương Dương                                      ', N'aPDaT5Bfj+oUB93S8mw+8Q==      ', N'thuongdthe150682@fpt.edu.vn                       ', N'Sleeping', N'0123456789          ', N'220220713220405335.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ', 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (3, N'Duong Thuong                                      ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'thuongduonghe150682@gmail.com                     ', N'I like tree', N'0123456789          ', NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (5, N'Admin                                             ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'Admin@gmail.com                                   ', N'Manage User', NULL, NULL, 1)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (6, N'Admin2                                            ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'Admin2@gmail.com                                  ', N'Manage Quiz', NULL, NULL, 2)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (7, N'Thuong Test                                       ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'thuongduongthi20022001@gmail.com                  ', NULL, NULL, NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (8, N'User1                                             ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'user1@gmail.com                                   ', N'I''m 20 years old', N'0123123123          ', N'820220719014528951.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ', 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (9, N'User2                                             ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'Ususer2@gmail.com                                 ', NULL, NULL, NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (10, N'User3                                             ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'User3@gmail.com                                   ', NULL, NULL, NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (11, N'User4                                             ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'User4@gmail.com                                   ', NULL, NULL, NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (12, N'User5                                             ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'User5@gmail.com                                   ', NULL, NULL, NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (13, N'AdminUser1                                        ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'AdminUser1@gmail.com                              ', NULL, NULL, NULL, 1)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (14, N'AdminUser2                                        ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'AdminUser2@gmail.com                              ', NULL, NULL, NULL, 1)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (15, N'AdminQuiz1                                        ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'AdminQuiz1@gmail.com                              ', NULL, NULL, NULL, 2)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (16, N'AdminQuiz2                                        ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'AdminQuiz2@gmail.com                              ', NULL, NULL, NULL, 2)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (17, N'Mui                                               ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'mui@gmail.com                                     ', NULL, NULL, NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (22, N'ABC                                               ', N'gKKKoMOjWtHwF22xsbt+Ow==      ', N'abc@gmail.com                                     ', NULL, NULL, NULL, 0)
INSERT [dbo].[User] ([userid], [username], [password], [email], [aboutme], [phone], [avatar], [role_id]) VALUES (23, N'QP1                                               ', N'5W+3FAj6vzMIqhvetzkLEQ==      ', N'ngocttha150097@fpt.edu.vn                         ', NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Option] ADD  CONSTRAINT [DF__Option__right_op__4D94879B]  DEFAULT ((0)) FOR [right_option]
GO
ALTER TABLE [dbo].[Option_test] ADD  CONSTRAINT [DF__Option_te__right__4E88ABD4]  DEFAULT ((0)) FOR [right_option]
GO
ALTER TABLE [dbo].[Option_test] ADD  CONSTRAINT [DF__Option_te__optio__4F7CD00D]  DEFAULT ((0)) FOR [option_status]
GO
ALTER TABLE [dbo].[Question_test] ADD  CONSTRAINT [DF__Question___quest__5070F446]  DEFAULT ((0)) FOR [question_status]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF__User__role_id__6C190EBB]  DEFAULT ((0)) FOR [role_id]
GO
ALTER TABLE [dbo].[category_Quiz]  WITH CHECK ADD  CONSTRAINT [pk_catequiz] FOREIGN KEY([quizid])
REFERENCES [dbo].[Quiz] ([quizid])
GO
ALTER TABLE [dbo].[category_Quiz] CHECK CONSTRAINT [pk_catequiz]
GO
ALTER TABLE [dbo].[category_Quiz]  WITH CHECK ADD  CONSTRAINT [pk_quizcate] FOREIGN KEY([categoryid])
REFERENCES [dbo].[Category] ([categoryid])
GO
ALTER TABLE [dbo].[category_Quiz] CHECK CONSTRAINT [pk_quizcate]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [pk_class_createid] FOREIGN KEY([createid])
REFERENCES [dbo].[User] ([userid])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [pk_class_createid]
GO
ALTER TABLE [dbo].[Class_User]  WITH CHECK ADD  CONSTRAINT [pk_class_class] FOREIGN KEY([classid])
REFERENCES [dbo].[Class] ([classid])
GO
ALTER TABLE [dbo].[Class_User] CHECK CONSTRAINT [pk_class_class]
GO
ALTER TABLE [dbo].[Class_User]  WITH CHECK ADD  CONSTRAINT [pk_class_user] FOREIGN KEY([userid])
REFERENCES [dbo].[User] ([userid])
GO
ALTER TABLE [dbo].[Class_User] CHECK CONSTRAINT [pk_class_user]
GO
ALTER TABLE [dbo].[ClassLog]  WITH CHECK ADD  CONSTRAINT [pk_log_class] FOREIGN KEY([classid])
REFERENCES [dbo].[Class] ([classid])
GO
ALTER TABLE [dbo].[ClassLog] CHECK CONSTRAINT [pk_log_class]
GO
ALTER TABLE [dbo].[Option]  WITH CHECK ADD  CONSTRAINT [FK_Option_Question] FOREIGN KEY([quizid], [questionid])
REFERENCES [dbo].[Question] ([quizid], [questionid])
GO
ALTER TABLE [dbo].[Option] CHECK CONSTRAINT [FK_Option_Question]
GO
ALTER TABLE [dbo].[Option]  WITH CHECK ADD  CONSTRAINT [pk_optquiz] FOREIGN KEY([quizid])
REFERENCES [dbo].[Quiz] ([quizid])
GO
ALTER TABLE [dbo].[Option] CHECK CONSTRAINT [pk_optquiz]
GO
ALTER TABLE [dbo].[Option_test]  WITH CHECK ADD  CONSTRAINT [FK_Option_test_Question_test] FOREIGN KEY([testid], [questionid])
REFERENCES [dbo].[Question_test] ([testid], [questionid])
GO
ALTER TABLE [dbo].[Option_test] CHECK CONSTRAINT [FK_Option_test_Question_test]
GO
ALTER TABLE [dbo].[Option_test]  WITH CHECK ADD  CONSTRAINT [pk_opttest] FOREIGN KEY([testid])
REFERENCES [dbo].[Test] ([testid])
GO
ALTER TABLE [dbo].[Option_test] CHECK CONSTRAINT [pk_opttest]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [pk_quesquiz] FOREIGN KEY([quizid])
REFERENCES [dbo].[Quiz] ([quizid])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [pk_quesquiz]
GO
ALTER TABLE [dbo].[Question_test]  WITH CHECK ADD  CONSTRAINT [pk_questtest] FOREIGN KEY([testid])
REFERENCES [dbo].[Test] ([testid])
GO
ALTER TABLE [dbo].[Question_test] CHECK CONSTRAINT [pk_questtest]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Class] FOREIGN KEY([classid])
REFERENCES [dbo].[Class] ([classid])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Class]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [pk_quizuser] FOREIGN KEY([userid])
REFERENCES [dbo].[User] ([userid])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [pk_quizuser]
GO
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [pk_ratequiz] FOREIGN KEY([quizid])
REFERENCES [dbo].[Quiz] ([quizid])
GO
ALTER TABLE [dbo].[Rate] CHECK CONSTRAINT [pk_ratequiz]
GO
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [pk_rateuser] FOREIGN KEY([userid])
REFERENCES [dbo].[User] ([userid])
GO
ALTER TABLE [dbo].[Rate] CHECK CONSTRAINT [pk_rateuser]
GO
ALTER TABLE [dbo].[Test]  WITH CHECK ADD  CONSTRAINT [pk_testquiz] FOREIGN KEY([quizid])
REFERENCES [dbo].[Quiz] ([quizid])
GO
ALTER TABLE [dbo].[Test] CHECK CONSTRAINT [pk_testquiz]
GO
ALTER TABLE [dbo].[Test]  WITH CHECK ADD  CONSTRAINT [pk_testuser] FOREIGN KEY([userid])
REFERENCES [dbo].[User] ([userid])
GO
ALTER TABLE [dbo].[Test] CHECK CONSTRAINT [pk_testuser]
GO
ALTER TABLE [dbo].[View]  WITH CHECK ADD  CONSTRAINT [pk_viewquiz] FOREIGN KEY([quizid])
REFERENCES [dbo].[Quiz] ([quizid])
GO
ALTER TABLE [dbo].[View] CHECK CONSTRAINT [pk_viewquiz]
GO
ALTER TABLE [dbo].[View]  WITH CHECK ADD  CONSTRAINT [pk_viewuser] FOREIGN KEY([userid])
REFERENCES [dbo].[User] ([userid])
GO
ALTER TABLE [dbo].[View] CHECK CONSTRAINT [pk_viewuser]
GO
USE [master]
GO
ALTER DATABASE [ProjectSWP_KS04] SET  READ_WRITE 
GO
