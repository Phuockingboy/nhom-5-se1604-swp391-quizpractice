/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author fptshop
 */
public class Quiz_Viewer {
    private int userid;
    private String email;
    private int quizid;
    private String title;
    private  int numOfViewers;

    public Quiz_Viewer(int userid, String email, int quizid, String title, int numOfViewers) {
        this.userid = userid;
        this.email = email;
        this.quizid = quizid;
        this.title = title;
        this.numOfViewers = numOfViewers;
    }
    
    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getQuizid() {
        return quizid;
    }

    public void setQuizid(int quizid) {
        this.quizid = quizid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumOfViewers() {
        return numOfViewers;
    }

    public void setNumOfViewers(int numOfViewers) {
        this.numOfViewers = numOfViewers;
    }
    
    
}
