/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author fptshop
 */
public class Class {
    private int classid;
    private int userid;
    private String classname;
    private String code;
    private boolean allow;

    public Class(int userid, String classname, String code, boolean allow) {
        this.userid = userid;
        this.classname = classname;
        this.code = code;
        this.allow = allow;
    }

    public Class(int classid, int userid, String classname, String code, boolean allow) {
        this.classid = classid;
        this.userid = userid;
        this.classname = classname;
        this.code = code;
        this.allow = allow;
    }
    
    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isAllow() {
        return allow;
    }

    public void setAllow(boolean allow) {
        this.allow = allow;
    }
    
}
