/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author MSI
 */
public class TestResult {
    private int testid,questionid,optionid;
    private String questionTitle, optionContent;
    private int numberOfRightAnswer;
    private boolean rightOption;
    private boolean optionStatus;

    public TestResult() {
    }

    public TestResult(int testid, int questionid, int optionid, String questionTitle, String optionContent, int numberOfRightAnswer, boolean rightOption, boolean optionStatus) {
        this.testid = testid;
        this.questionid = questionid;
        this.optionid = optionid;
        this.questionTitle = questionTitle;
        this.optionContent = optionContent;
        this.numberOfRightAnswer = numberOfRightAnswer;
        this.rightOption = rightOption;
        this.optionStatus = optionStatus;
    }

    public int getTestid() {
        return testid;
    }

    public void setTestid(int testid) {
        this.testid = testid;
    }

    public int getQuestionid() {
        return questionid;
    }

    public void setQuestionid(int questionid) {
        this.questionid = questionid;
    }

    public int getOptionid() {
        return optionid;
    }

    public void setOptionid(int optionid) {
        this.optionid = optionid;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getOptionContent() {
        return optionContent;
    }

    public void setOptionContent(String optionContent) {
        this.optionContent = optionContent;
    }

    public int getNumberOfRightAnswer() {
        return numberOfRightAnswer;
    }

    public void setNumberOfRightAnswer(int numberOfRightAnswer) {
        this.numberOfRightAnswer = numberOfRightAnswer;
    }

    public boolean isRightOption() {
        return rightOption;
    }

    public void setRightOption(boolean rightOption) {
        this.rightOption = rightOption;
    }

    public boolean isOptionStatus() {
        return optionStatus;
    }

    public void setOptionStatus(boolean optionStatus) {
        this.optionStatus = optionStatus;
    }

    @Override
    public String toString() {
        return "TestResult{" + "testid=" + testid + ", questionid=" + questionid + ", optionid=" + optionid + ", questionTitle=" + questionTitle + ", optionContent=" + optionContent + ", numberOfRightAnswer=" + numberOfRightAnswer + ", rightOption=" + rightOption + ", optionStatus=" + optionStatus + '}';
    }
    
}
