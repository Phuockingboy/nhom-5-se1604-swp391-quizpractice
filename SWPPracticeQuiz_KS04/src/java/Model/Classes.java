
package Model;

import java.sql.Date;

/**
 *
 * @author HP
 */
public class Classes {
    private int classid;
    private int createid;
    private String name;
    private Date date_create;
    private String code;
    private boolean allowMemberCreateQuiz;
    private String username;
    private int numberOfMember;
    public Classes() {
    }

    public Classes(int classid, int createid, String name, Date date_create, String Code, boolean allowMemberCreateQuiz) {
        this.classid = classid;
        this.createid = createid;
        this.name = name;
        this.date_create = date_create;
        this.code = Code;
        this.allowMemberCreateQuiz = allowMemberCreateQuiz;
    }

    public Classes(int classid, int createid, String name, Date date_create, String code, boolean allowMemberCreateQuiz, String username, int numberOfMember) {
        this.classid = classid;
        this.createid = createid;
        this.name = name;
        this.date_create = date_create;
        this.code = code;
        this.allowMemberCreateQuiz = allowMemberCreateQuiz;
        this.username = username;
        this.numberOfMember = numberOfMember;
    }
    public Classes(int classid, int createid, String name, Date date_create, String code, boolean allowMemberCreateQuiz, String username) {
        this.classid = classid;
        this.createid = createid;
        this.name = name;
        this.date_create = date_create;
        this.code = code;
        this.allowMemberCreateQuiz = allowMemberCreateQuiz;
        this.username = username;
    }
    
    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public int getCreateid() {
        return createid;
    }

    public void setCreateid(int createid) {
        this.createid = createid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String Code) {
        this.code = Code;
    }

    public boolean isAllowMemberCreateQuiz() {
        return allowMemberCreateQuiz;
    }

    public void setAllowMemberCreateQuiz(boolean allowMemberCreateQuiz) {
        this.allowMemberCreateQuiz = allowMemberCreateQuiz;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getNumberOfMember() {
        return numberOfMember;
    }

    public void setNumberOfMember(int numberOfMember) {
        this.numberOfMember = numberOfMember;
    }

    @Override
    public String toString() {
        return "Class{" + "classid=" + classid + ", createid=" + createid + ", name=" + name + ", date_create=" + date_create + ", Code=" + code + ", allowMemberCreateQuiz=" + allowMemberCreateQuiz + '}';
    }
    
}
