/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author dangm
 */
public class QuestionTest {

    int testid;
    int questionid;
    String questionTitle;
    String instruction;
//    List<OptionTest> list_test_result;
    boolean questionStatus;
    int numberOfRightAnswer;
    boolean done;
    boolean randomOption;

    List<OptionTest> list_optionTest;
 

    public QuestionTest() {
    }

    public QuestionTest(int testid, int questionid, String questionTitle, String instruction, boolean questionStatus, int numberOfRightAnswer, List<OptionTest> list_optionTest) {
        this.testid = testid;
        this.questionid = questionid;
        this.questionTitle = questionTitle;
        this.instruction = instruction;
        this.questionStatus = questionStatus;
        this.numberOfRightAnswer = numberOfRightAnswer;
        this.list_optionTest = list_optionTest;
    }

    public QuestionTest(int testid, int questionid, String questionTitle, String instruction, boolean questionStatus, int numberOfRightAnswer) {
        this.testid = testid;
        this.questionid = questionid;
        this.questionTitle = questionTitle;
        this.instruction = instruction;
        this.questionStatus = questionStatus;
        this.numberOfRightAnswer = numberOfRightAnswer;
    }

    public QuestionTest(int testid, int questionid, String questionTitle, String instruction, boolean questionStatus) {
        this.testid = testid;
        this.questionid = questionid;
        this.questionTitle = questionTitle;
        this.instruction = instruction;
        this.questionStatus = questionStatus;
    }

    public QuestionTest(int testid, int questionid, String questionTitle, String instruction, int numberOfRightAnswer, boolean done, boolean questionStatus) {
        this.testid = testid;
        this.questionid = questionid;
        this.questionTitle = questionTitle;
        this.instruction = instruction;
        this.questionStatus = questionStatus;
        this.numberOfRightAnswer = numberOfRightAnswer;
        this.done = done;
    }
    
    public QuestionTest(int testid, int questionid, String questionTitle, String instruction, int numberOfRightAnswer) {
        this.testid = testid;
        this.questionid = questionid;
        this.questionTitle = questionTitle;
        this.instruction = instruction;
        this.numberOfRightAnswer = numberOfRightAnswer;
    }
    

    public QuestionTest(int testid, int questionid, String questionTitle, String instruction) {
        this.testid = testid;
        this.questionid = questionid;
        this.questionTitle = questionTitle;
        this.instruction = instruction;
    }
    
    public int getTestid() {
        return testid;
    }

    public void setTestid(int testid) {
        this.testid = testid;
    }
    

    public int getQuestionid() {
        return questionid;
    }

    public void setQuestionid(int questionid) {
        this.questionid = questionid;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public boolean isQuestionStatus() {
        return questionStatus;
    }

    public void setQuestionStatus(boolean questionStatus) {
        this.questionStatus = questionStatus;
    }

    public int getNumberOfRightAnswer() {
        return numberOfRightAnswer;
    }

    public void setNumberOfRightAnswer(int numberOfRightAnswer) {
        this.numberOfRightAnswer = numberOfRightAnswer;
    }

    public List<OptionTest> getList_optionTest() {
        return list_optionTest;
    }

    public void setList_optionTest(List<OptionTest> list_optionTest) {
        this.list_optionTest = list_optionTest;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isRandomOption() {
        return randomOption;
    }

    public void setRandomOption(boolean randomOption) {
        this.randomOption = randomOption;
    }
    
    @Override
    public String toString() {
        return "QuestionTest{" + "testid=" + testid + ", questionid=" + questionid + ", questionTitle=" + questionTitle + ", instruction=" + instruction + ", questionStatus=" + questionStatus + ", numberOfRightAnswer=" + numberOfRightAnswer + ", list_optionTest=" + list_optionTest + '}';
    }
    
    
    

}
