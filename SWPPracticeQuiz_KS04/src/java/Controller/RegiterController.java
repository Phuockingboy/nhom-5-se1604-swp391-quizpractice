
package Controller;

import DAO.UserDAO;
import Model.Register;
import Model.User;
import Validation.AES;
import Validation.Utilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "RegiterController", urlPatterns = {"/RegiterController"})
public class RegiterController extends HttpServlet {

    
    final String secretKeyy = "sssskkkkkkkkkhhhhh!!!!!!";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String submit = request.getParameter("submit");
            UserDAO dao = new UserDAO();
            if (submit == null) {
                dispath(request, response, "register.jsp");
            } else {
                String userName = request.getParameter("userName");
                String password = request.getParameter("password");
                String rePassword = request.getParameter("rePassword");
                String email = request.getParameter("email");
                String phone = request.getParameter("phone");
                User u ;
                u = dao.checkAccountExist(email);
                if (!rePassword.equalsIgnoreCase(password)) {
                    request.setAttribute("message1", "The re-entered password does not match the entered password!");
                    dispath(request, response, "register.jsp");
                } else {
                    if (u == null) {
                        String code = Utilities.generateRandomCode();
                        //Encrypt the password
                        String encrytPassword = AES.encrypt(password, secretKeyy);
                        //User user = new User(ID, userName, password, email, "", phone, "", 0);
                        Register register = new Register(userName,encrytPassword , email, phone, 0, code);
                        String Subject = "Create account QuizPractice";
                        String msg = "Registered successfully.Please verify your account using this code: " + code;
                        boolean test = Utilities.sendEmail(email, Subject, msg);
                        if (test == true) {
                            HttpSession session = request.getSession();
                            session.setAttribute("authcode", register);
                            dispath(request, response, "verifyRegister.jsp");
                        } else {
                            request.setAttribute("message2", "Failed to send verification email");
                            dispath(request, response, "register.jsp");
                        }
                    } else {
                        request.setAttribute("message2", "Email already exists, please use another email!");
                        //dispath(request, response, "/JSP/register.jsp");
                        request.getRequestDispatcher("register.jsp").forward(request, response);
                    }
                }

            }

        }
    }

    public void dispath(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
        RequestDispatcher dispath = request.getRequestDispatcher(page);
        dispath.forward(request, response);
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(RegiterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(RegiterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
