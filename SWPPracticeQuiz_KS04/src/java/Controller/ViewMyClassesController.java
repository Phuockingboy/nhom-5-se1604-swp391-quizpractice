
package Controller;

import DAO.ClassDAO;
import DAO.QuizDAO;
import DAO.UserDAO;
import Model.Classes;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "ViewMyClassesController", urlPatterns = {"/ViewMyClassesController"})
public class ViewMyClassesController extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String txtSearch = request.getParameter("txtSearch");
        if(txtSearch==null || txtSearch.equals("") ){
            txtSearch="";
        }
        if(user==null){
            request.setAttribute("message", "You need to login before view class.");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        }else{
            String task = request.getParameter("task");
            System.out.println(task);
            if(task==null || task.equals("") || task.equals("null") ){
                task="viewAll";
            }
            System.out.println(task);
            ClassDAO classdao = new ClassDAO();
            List<Classes> listClass;
            switch (task){
                case "viewAll":
                    listClass = classdao.getClassIfUserIsMember(user.getId(),txtSearch);
                    System.out.println(listClass.size());
                    break;
                case "viewCreated":
                    listClass = classdao.getClassCreateByUser(user.getId(),txtSearch);
                    break;
                case "viewAdmin":
                    listClass = classdao.getClassIfUserIsAdmin(user.getId(),txtSearch);
                    break;
                default:
                    listClass = classdao.getClassIfUserIsMember(user.getId(),txtSearch);
                    break;
            }
            QuizDAO dao = new QuizDAO();
            int countNumberOfClass = classdao.countClassOfUser(user.getId());
            UserDAO udao = new UserDAO();
            user = udao.getUserById(user.getId()+"");
            int numberOfQuiz = dao.getTotalOwnQuiz(user.getId(),"");
            request.setAttribute("task", task);
            request.setAttribute("txtSearch", txtSearch);
            request.setAttribute("numberOfQuiz", numberOfQuiz);
            request.setAttribute("countNumberOfClass", countNumberOfClass);
            request.setAttribute("listClass", listClass);
            request.getRequestDispatcher("myclass.jsp").forward(request, response);
        }
               
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewMyClassesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewMyClassesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
