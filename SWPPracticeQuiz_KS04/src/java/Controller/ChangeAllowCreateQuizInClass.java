package Controller;

import DAO.ClassDAO;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ChangeAllowCreateQuizInClass", urlPatterns = {"/ChangeAllowCreateQuizInClass"})
public class ChangeAllowCreateQuizInClass extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            request.setAttribute("message", "You need to login before change allow member create quiz in your class");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        } else {
            String classid = request.getParameter("classid");
            String allow = request.getParameter("allow");
            System.out.println("-----------------");
            System.out.println(allow);
            System.out.println("-----------------");
            ClassDAO classdao = new ClassDAO();
            boolean check = classdao.changeAllowCreateQuiz(classid, allow);
            if (check) {
                request.setAttribute("message", "Change Allow Member Create Quiz: Successfully.");
                request.setAttribute("classid", classid);
                request.getRequestDispatcher("ViewQuizInClassController").forward(request, response);
            } else {
                request.setAttribute("message", "Erorr.");
                request.setAttribute("classid", classid);
                request.getRequestDispatcher("ViewQuizInClassController").forward(request, response);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ChangeAllowCreateQuizInClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ChangeAllowCreateQuizInClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
