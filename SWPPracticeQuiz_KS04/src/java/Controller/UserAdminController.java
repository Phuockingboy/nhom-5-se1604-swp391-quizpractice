/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.UserAdminDAO;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdk.nashorn.internal.ir.BreakNode;

/**
 *
 * @author dangm
 */
@WebServlet(name = "UserAdminController", urlPatterns = {"/UserAdminController"})
public class UserAdminController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            UserAdminDAO dao = new UserAdminDAO();
            String action = request.getParameter("Action");
            if (action != null) {
                String userid = request.getParameter("userId");
                switch (action) {
                    case "Ban":
                        if(dao.banUser(userid)){
                            request.setAttribute("message", "Ban user successfully!");
                        }
                        ;
                        break;
                    case "Unban":
                        if(dao.unBanUser(userid)){
                            request.setAttribute("message", "Unban user successfully!");
                        }
                        ;
                        break;
                    default:
                        break;
                }
            }
            String username = request.getParameter("Search");
            List<User> listUser = null;
//            System.out.println(username);
            if (username != null) {
                listUser = dao.getUserByUsername(username);
            } else {
                listUser = dao.getUser();
            }

            request.setAttribute("numUser", dao.countUser());
            request.setAttribute("numQuiz", dao.countQuiz());
            request.setAttribute("numCategory", dao.countCategory());
            request.setAttribute("numClass", dao.countClass());
            request.setAttribute("listUser", listUser);
            String thisweek = dao.numVisitorInWeek(LocalDate.now());
            String lastweek = dao.numVisitorInWeek(LocalDate.now().minusDays(7));
            request.setAttribute("lastweekVisit", lastweek);
            request.setAttribute("thisweekVisit", thisweek);
//                request.getRequestDispatcher("index.html").forward(request, response);
            request.getRequestDispatcher("UserAdmin.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String action = request.getParameter("Action");
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
