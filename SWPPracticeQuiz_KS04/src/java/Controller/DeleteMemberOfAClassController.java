package Controller;

import DAO.ClassDAO;
import Model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "DeleteMemberOfAClassController", urlPatterns = {"/DeleteMemberOfAClassController"})
public class DeleteMemberOfAClassController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String classid = request.getParameter("classid");
        String userid = request.getParameter("userid");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            request.setAttribute("message", "You need to login before delete a member. ");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        } else {
            ClassDAO classdao = new ClassDAO();
            boolean check = classdao.removeMemberInClass(userid, classid);
            if (check) {
                request.setAttribute("messageDelete", "Delete members successfully.");
            } else {
                request.setAttribute("messageDelete", "Error to delete member. Please check again.");
            }
            request.setAttribute("classid", classid);
            request.getRequestDispatcher("ViewMemberInClassController").forward(request, response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMemberOfAClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMemberOfAClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
