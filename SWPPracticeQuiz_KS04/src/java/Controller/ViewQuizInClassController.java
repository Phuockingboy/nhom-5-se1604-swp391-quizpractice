package Controller;

import DAO.ClassDAO;
import DAO.UserDAO;
import Model.Classes;
import Model.QuizUser;
import Model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ViewQuizInClassController", urlPatterns = {"/ViewQuizInClassController"})
public class ViewQuizInClassController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            request.setAttribute("message", "You need to login before in a class.");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        } else {
            String txtSearch = request.getParameter("txtSearch");
            if (txtSearch == null || txtSearch.equals("")) {
                txtSearch = "";
            }
            ClassDAO classdao = new ClassDAO();
            UserDAO udao = new UserDAO();
            String classid = (String) (request.getAttribute("classid") + "");
            if ("null".equals(classid) || "".equals(classid) || classid == null) {
                classid = request.getParameter("classid");
            }
//            System.out.println(classid);
            int classidd = Integer.parseInt(classid);
            boolean check = classdao.checkClassExistById(classidd);
            if (!check) {
                request.getRequestDispatcher("ClassError.jsp").forward(request, response);
            } else {
                String message = (String) request.getAttribute("message");
                String task = "viewAllQuiz";
                if (request.getParameter("task") != null && !"".equals(request.getParameter("task"))
                        && !"null".equals(request.getParameter("task"))) {
                    task = request.getParameter("task");
                    request.setAttribute("task", task);
                }
                List<QuizUser> listQuiz = classdao.getAllQuizInClass(classidd,txtSearch);
                if (task.equals("viewOwnQuizInClass")) {
                    listQuiz = classdao.getAllQuizInClassCreateByUserid(classidd, user.getId(),txtSearch);
                }
                Classes classOfId = classdao.getClassbyClassid(classidd);
                int totalMember = classdao.countMemberOfClass(classidd);
                User creater = udao.getUserById(classOfId.getCreateid() + "");
                User currentUser = udao.getUserById(user.getId() + "");
                boolean isAdmin = classdao.checkIfUserIsAdminInClass(classidd, user.getId());

                request.setAttribute("message", message);
                request.setAttribute("listQuiz", listQuiz);
                request.setAttribute("currentUser", currentUser);
                request.setAttribute("isAdmin", isAdmin);
                request.setAttribute("totalMember", totalMember);
                request.setAttribute("classOfId", classOfId);
                request.setAttribute("creater", creater);
                request.setAttribute("task", task);
                request.setAttribute("txtSearch", txtSearch);
                request.setAttribute("classid", classid);
                String messageDelete = (String) request.getAttribute("messageDelete");
                request.setAttribute("messageDelete", messageDelete);
                request.getRequestDispatcher("viewQuizInClass.jsp").forward(request, response);
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewQuizInClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ViewQuizInClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
