package DAO;

import Model.Category;
import Model.Option;
import Model.Question;
import Model.QuestionWithRightOption;
import Model.Quiz;
import Model.QuizUser;
import Model.User;
import Model.RateAndFeedback;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import sun.security.x509.X500Name;

public class RateAndFeedBackDAO {

    public int getSumOfRateByQuizId(int quizid) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int sum = 0;
        try {
            conn = new DBContext().connection;
            String query = "SELECT Sum(rate) from [ProjectSWP_KS04].[dbo].[Rate] where quizid=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                sum = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sum;
    }
    
    public int getNumOfRateByQuizId(int quizid) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int num = 0;
        try {
            conn = new DBContext().connection;
            String query = "SELECT Count(rate) from [ProjectSWP_KS04].[dbo].[Rate]\n"
                    + "where quizid=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return num;
    }

    public boolean addRateAndFeedBack(int userid, int quizid, int rate, String feedback) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "INSERT INTO [dbo].[Rate]([userid],[quizid],[rate],[feedback])\n"
                    + "VALUES (?, ?, ?, ?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setInt(2, quizid);
            ps.setInt(3, rate);
            ps.setString(4, feedback);
            RateAndFeedBackDAO dao = new RateAndFeedBackDAO();
            RateAndFeedback check = dao.hasRateTheQuiz(userid, quizid);
            if (check == null) {
                ps.executeUpdate();
                return true;
            }
            return false;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    //get userid by quizid
    public int getUserIdByQuizId(int quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int userid = 0;
        try {
            conn = new DBContext().connection;
            String query = "SELECT userid FROM [Quiz] WHERE quizid = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                userid = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userid;
    }

    // check user has rate the accessed quiz or not
    public RateAndFeedback hasRateTheQuiz(int userid, int quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            String query = "SELECT * FROM RATE WHERE USERID = ? AND QUIZID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setInt(2, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new RateAndFeedback(rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getString(5));
            }
            return null;
        } catch (SQLException e) {
            return null;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // get list rate of quiz
    public List<RateAndFeedback> getListRateOfQuiz(int quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<RateAndFeedback> list = new ArrayList<>();
        try {
            conn = new DBContext().connection;
            String query = "SELECT r.userid,[quizid],[rate],[feedback],u.username, u.avatar\n"
                    + "FROM [ProjectSWP_KS04].[dbo].[Rate] as r, [User] as u\n"
                    + "where u.userid = r.userid and quizid=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new RateAndFeedback(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // get list rate of quiz
    public List<RateAndFeedback> getLAllRateOfQuiz() throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<RateAndFeedback> list = new ArrayList<>();
        try {
            conn = new DBContext().connection;
            String query = "SELECT"
                    + "      r.userid\n"
                    + "      ,[quizid]\n"
                    + "      ,[rate]\n"
                    + "      ,[feedback]\n"
                    + "	  ,u.username\n"
                    + "  FROM [ProjectSWP_KS04].[dbo].[Rate] as r, [User] as u\n"
                    + "  where u.userid = r.userid";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new RateAndFeedback(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public List<RateAndFeedback> getTop3RateOfQuiz(int quizid, int count) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<RateAndFeedback> list = new ArrayList<>();
        try {
            conn = new DBContext().connection;
            String query = "SELECT r.userid,[quizid],[rate],[feedback],u.username, u.avatar\n"
                    + "FROM [ProjectSWP_KS04].[dbo].[Rate] as r, [User] as u\n"
                    + "where u.userid = r.userid and quizid=? order by r.rateid\n"
                    + "offset ? row fetch next 3 row only";
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            ps.setInt(2, count);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new RateAndFeedback(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public void editRateAndFeedback(int userid, int quizid, int rate, String feedback) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "update [Rate] set rate=?, feedback=? where userid=? and quizid=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, rate);
            ps.setString(2, feedback);
            ps.setInt(3, userid);
            ps.setInt(4, quizid);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        RateAndFeedBackDAO dao = new RateAndFeedBackDAO();
        dao.addRateAndFeedBack(1, 2, 4, "329");
        System.out.println(dao.getUserIdByQuizId(10));
        List list = dao.getListRateOfQuiz(4);
        List list1 = dao.getLAllRateOfQuiz();
        for (int i = 0; i < list1.size(); i++) {
            System.out.println(list1.get(i).toString());
        }
        System.out.println((double) 1 / 4);
        double a = (double) 1 / 4;
        System.out.println((double) Math.round((double) 1 / 4 * 10) / 10);
        System.out.println(dao.getNumOfRateByQuizId(5));
        System.out.println(dao.getSumOfRateByQuizId(5));
    }
}
