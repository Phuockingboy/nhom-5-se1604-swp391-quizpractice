package DAO;

import Model.Category;
import Model.Option;
import Model.Question;
import Model.Quiz;
import Model.QuizUser;
import Model.Quiz_Viewer;
import Model.User;
import Model.RateAndFeedback;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QuizDAO {

    // get all own quiz in library
    public List<Quiz> getAllQuiz(int xUserid) throws SQLException {
        String query = "select * from [Quiz] where userid = ?";
        List<Quiz> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xUserid);
            rs = ps.executeQuery();
            while (rs.next()) {
                Quiz quiz = new Quiz(rs.getInt("quizid"), rs.getInt("userid"), rs.getString("title"),
                        rs.getString("Description"), rs.getDate("Date_Created"), rs.getDate("Last_update"));
                list.add(quiz);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // get all quiz
    public List<QuizUser> getAllQuiz() throws SQLException {
        String query = "select u.username,q.quizid,q.userid,q.title, q.[Description], q.Date_created, q.Last_update from [user] u, quiz q\n"
                + "where u.userid = q.userid and q.classid is null order by q.quizid desc";
        List<QuizUser> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt("quizid"), rs.getInt("userid"), rs.getString("username"),
                        rs.getString("title"),
                        rs.getString("Description"), rs.getDate("Date_Created"), rs.getDate("Last_update"));
                list.add(quiz);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // get all quiz by page and search
    public List<QuizUser> getAllQuizByPage(String index, String search) throws SQLException {
        String query = "select q.*, u.username from Quiz q join category_Quiz c\n"
                + "on q.quizid = c.quizid join Category t on \n"
                + "c.categoryid = t.categoryid join [User] u \n"
                + "on q.userid = u.userid where ( q.title like \n"
                + "? or t.category_name like ? ) and q.classid is null order by q.quizid desc\n"
                + "offset ? row fetch next 6 row only";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int page = Integer.parseInt(index);
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setInt(3, page * 6 - 6);
            rs = ps.executeQuery();
            List<QuizUser> list = new ArrayList<>();
            while (rs.next()) {
                QuizUser quizUser = new QuizUser(rs.getInt(1), rs.getInt(2), rs.getString(8), rs.getString(3),
                        rs.getString(4), rs.getDate(5), rs.getDate(6));
                list.add(quizUser);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    // get all quiz by page and search
    public List<QuizUser> getAllQuizByPageOfAUser(String index, String search, String userid) throws SQLException {
        String query = "select q.*, u.username from Quiz q join category_Quiz c\n"
                + "             on q.quizid = c.quizid join Category t on \n"
                + "                c.categoryid = t.categoryid join [User] u on\n"
                + "                q.userid = u.userid where (q.title like ?\n"
                + "               or t.category_name like ? ) and q.classid is null and u.userid = ? group by\n"
                + "			   q.quizid, q.title,q.Date_Created,q.[Description],\n"
                + "			   q.Last_update,q.classid,q.userid,u.username order by q.quizid desc\n"
                + "                 offset ? row fetch next 6 row only";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int page = Integer.parseInt(index);
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setString(3, userid);
            ps.setInt(4, page * 6 - 6);
            rs = ps.executeQuery();
            List<QuizUser> list = new ArrayList<>();
            while (rs.next()) {
                QuizUser quizUser = new QuizUser(rs.getInt(1), rs.getInt(2), rs.getString(8), rs.getString(3),
                        rs.getString(4), rs.getDate(5), rs.getDate(6));
                list.add(quizUser);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    // count all quiz of search
    public int countAllQuizSearch(String search) throws SQLException {
        String query = "select count(*) from Quiz q join category_Quiz c\n"
                + "                on q.quizid = c.quizid join Category t on\n"
                + "                c.categoryid = t.categoryid \n"
                + "                where (q.title like ? or t.category_name like ?) and q.classid is null";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    // get all quiz by page and search and category
    public List<QuizUser> getAllQuizByPageAndCategory(String index, String search, String category)
            throws SQLException {
        String query = "select q.*,u.username from Quiz q join category_Quiz c\n"
                + "on q.quizid = c.quizid join [User] u on q.userid = u.userid\n"
                + "where q.title like ? and q.classid is null and \n"
                + "c.categoryid=? order by q.quizid desc\n"
                + "offset ? row fetch next 6 row only";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int page = Integer.parseInt(index);
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, category);
            ps.setInt(3, page * 6 - 6);
            rs = ps.executeQuery();
            List<QuizUser> list = new ArrayList<>();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt(1), rs.getInt(2), rs.getString(8), rs.getString(3),
                        rs.getString(4), rs.getDate(5), rs.getDate(6));
                list.add(quiz);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    // get all quiz by page and search and category
    public List<QuizUser> getAllQuizByPageAndCategoryOfAUser(String index, String search, String category, String userid)
            throws SQLException {
        String query = "select q.*,u.username from Quiz q join category_Quiz c\n"
                + "                on q.quizid = c.quizid join [User] u on q.userid = u.userid\n"
                + "                where q.title like ? and q.classid is null and \n"
                + "                c.categoryid=? and u.userid = ?\n"
                + "				group by q.quizid, q.title,q.Date_Created,q.[Description],\n"
                + "				q.Last_update,q.classid,q.userid,u.username order by q.quizid desc\n"
                + "                offset ? row fetch next 6 row only";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int page = Integer.parseInt(index);
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, category);
            ps.setString(3, userid);
            ps.setInt(4, page * 6 - 6);
            rs = ps.executeQuery();
            List<QuizUser> list = new ArrayList<>();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt(1), rs.getInt(2), rs.getString(8), rs.getString(3),
                        rs.getString(4), rs.getDate(5), rs.getDate(6));
                list.add(quiz);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    // count all quiz by category and search
    public int countAllQuizByPageAndCategory(String search, String category) throws SQLException {
        String query = "select count(*) from Quiz q join category_Quiz c\n"
                + "on q.quizid = c.quizid where title like ? \n"
                + "and c.categoryid=? and q.classid is null";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, category);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public List<QuizUser> getAllQuizByCategoryCheckbox(String[] id) {
        String query = "select u.username,q.*\n"
                + "from [user] u, quiz q, category_quiz c\n"
                + "where u.userid = q.userid and q.quizid = c.quizid ";
        List<QuizUser> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (id != null) {
            query += "and c.categoryid in (";
            for (int i = 0; i < id.length; i++) {
                query += id[i] + ",";
            }
        }
        if (query.endsWith(",")) {
            query = query.substring(0, query.length() - 1);
            query += ")";
        }
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt("quizid"), rs.getInt("userid"), rs.getString("username"),
                        rs.getString("title"),
                        rs.getString("Description"), rs.getDate("Date_Created"), rs.getDate("Last_update"));
                list.add(quiz);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(query);
        return list;
    }

    // get username by userid
    public String getUsernameByUserid(int userid) throws SQLException {
        String query = "select username from [User]"
                + "where userid = ?";
        List<QuizUser> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String userName = "";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                userName = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return userName;
    }

    public List<QuizUser> getAllQuizByTitle(String title) throws SQLException {
        String query = "select u.username,q.*\n"
                + "from [user] u, quiz q\n"
                + "where u.userid = q.userid and q.classid is null and q.title like'%" + title + "%'";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<QuizUser> list = new ArrayList<>();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt("quizid"), rs.getInt("userid"), rs.getString("username"),
                        rs.getString("title"),
                        rs.getString("Description"), rs.getDate("Date_Created"), rs.getDate("Last_update"));
                list.add(quiz);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    // count total quiz in own library
    public int getTotalOwnQuiz(int xUserid, String search) throws SQLException {
        String query = "select count(*) from [Quiz] where userid = ? and title like ? and classid is null";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int numOfQuiz = 0;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xUserid);
            ps.setString(2, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                numOfQuiz = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return numOfQuiz;
    }

    // get total quiz select by userid and categoryid
    public int getTotalOwnQuizByCategory(int xUserid, String categoryid, String search) throws SQLException {
        String query = "select count(*) from Quiz q join category_Quiz c on q.quizid = c.quizid\n"
                + "where q.userid = ?  and c.categoryid = ? and q.title like ? and q.classid is null";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int numOfQuiz = 0;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xUserid);
            ps.setString(2, categoryid);
            ps.setString(3, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                numOfQuiz = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return numOfQuiz;
    }

    // get other quiz of a user by userid and quizid
    public List<QuizUser> getListOtherQuizOfUser(String userid, String quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            String query = "select u.username, q.* from Quiz q join [User] u\n"
                    + "on q.userid = u.userid\n"
                    + "where q.userid=? and q.quizid not in (?)";
            ps = conn.prepareStatement(query);
            ps.setString(1, userid);
            ps.setString(2, quizid);
            List<QuizUser> list = new ArrayList<>();
            rs = ps.executeQuery();
            while (rs.next()) {
                QuizUser quizuser = new QuizUser(rs.getInt(3), rs.getInt(1), rs.getString(2), rs.getString(5),
                        rs.getString(6), rs.getDate(7), rs.getDate(8));
                list.add(quizuser);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    // get all own quiz in library with pagination
    public List<Quiz> quizPagination(int page, int xUserid) throws SQLException {
        List<Quiz> list = new ArrayList<>();
        String query = "select * from Quiz where userid=? order by quizid desc\n"
                + "offset ? row fetch next 6 row only";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xUserid);
            ps.setInt(2, (page - 1) * 6);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt(1), rs.getInt(2), rs.getString(3),
                        rs.getString(4), rs.getDate(5), rs.getDate(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // get all own quiz in library with pagination
    public List<Quiz> quizPagination(int page, int xUserid, String search) throws SQLException {
        List<Quiz> list = new ArrayList<>();
        String query = "select * from Quiz where userid = ? \n"
                + "and title like ? and classid is null order by quizid desc \n"
                + "offset ? row fetch next 6 row only";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xUserid);
            ps.setString(2, "%" + search + "%");
            ps.setInt(3, (page - 1) * 6);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt(1), rs.getInt(2), rs.getString(3),
                        rs.getString(4), rs.getDate(5), rs.getDate(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // get all own quiz in library with pagination by category
    // public List<Quiz> quizPagination(int page, int xUserid, String categoryid)
    // throws SQLException {
    // List<Quiz> list = new ArrayList<>();
    // String query = "select * from Quiz q join category_Quiz c on q.quizid =
    // c.quizid\n"
    // + "where q.userid = ? and c.categoryid = ? order by q.quizid desc\n"
    // + " offset ? row fetch next 6 row only";
    // Connection conn = null;
    // PreparedStatement ps = null;
    // ResultSet rs = null;
    // try {
    // conn = new DBContext().connection;
    // ps = conn.prepareStatement(query);
    // ps.setInt(1, xUserid);
    // ps.setString(2, categoryid);
    // ps.setInt(3, (page - 1) * 6);
    // rs = ps.executeQuery();
    // while (rs.next()) {
    // list.add(new Quiz(rs.getInt(1), rs.getInt(2), rs.getString(3),
    // rs.getString(4), rs.getDate(5), rs.getDate(6)));
    // }
    // } catch (SQLException e) {
    // e.printStackTrace();
    // } finally {
    // if (rs != null) {
    // rs.close();
    // }
    // if (ps != null) {
    // ps.close();
    // }
    // if (conn != null) {
    // conn.close();
    // }
    // }
    // return list;
    // }
    // get all own quiz in library with pagination by category
    public List<Quiz> quizPagination(int page, int xUserid, String categoryid, String search) throws SQLException {
        List<Quiz> list = new ArrayList<>();
        String query = "select * from Quiz q join category_Quiz c on q.quizid = c.quizid\n"
                + "where q.userid = ?  and c.categoryid = ? and q.title like ? and q.classid is null order by q.quizid desc\n"
                + "        offset ? row fetch next 6 row only";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xUserid);
            ps.setString(2, categoryid);
            ps.setString(3, "%" + search + "%");
            ps.setInt(4, (page - 1) * 6);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt(1), rs.getInt(2), rs.getString(3),
                        rs.getString(4), rs.getDate(5), rs.getDate(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // search quiz in library
    public List<Quiz> searchQuizInLibrary(int xUserid, String words) throws SQLException {
        List<Quiz> listBefore = getAllQuiz(xUserid);
        List<Quiz> listAfterSearch = new ArrayList<>();
        // loop for traverse all quiz in list quiz (library)
        for (int i = 0; i < listBefore.size(); i++) {
            if (listBefore.get(i).getTitle().toLowerCase().contains(words.toLowerCase())) {
                listAfterSearch.add(listBefore.get(i));
            }
        }
        return listAfterSearch;
    }

    public List<Quiz> searchQuizInLibrary2(int xUserid, String words) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from Quiz where userid = ?\n"
                + "and title like ?";
        try {
            List<Quiz> list = new ArrayList<>();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xUserid);
            ps.setString(2, "%" + words + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Quiz q = new Quiz(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getDate(5),
                        rs.getDate(6));
                list.add(q);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public String getTitleByQuizId(int xQuizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "Select title from [Quiz] where quizid = ?";
        try {
            String xTitle = "";
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xQuizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                xTitle = rs.getString(1);
            }
            return xTitle;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public List<Option> getAllOptions(int xQuestionId) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select optionid, option_content, right_option from  [Option] \n"
                + "where questionid = ? \n";
        try {
            List<Option> listOption = new ArrayList<>();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, xQuestionId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Option op = new Option(rs.getInt(1), rs.getString(2), rs.getBoolean(3));
                listOption.add(op);
            }
            return listOption;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    // insert into quiz table new quiz by userid
    public boolean insertNewQuizByUserid(int userid, String title, String description) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "insert into Quiz (userid, title, [Description],Date_Created,classid)\n"
                    + "values (?,?,?, GETDATE(),null)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setString(2, title);
            ps.setString(3, description);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    // insert into quiz table new quiz by userid

    public boolean insertNewQuizByUseridAndClassid(int userid, String title, String description, int classid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "insert into Quiz (userid, title, [Description],Date_Created,classid)\n"
                    + "values (?,?,?, GETDATE(),?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setString(2, title);
            ps.setString(3, description);
            ps.setInt(4, classid);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    // get last quiz create by userid
    public int getLassQuizByUser(int userid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String query = "select top 1* from Quiz where userid=? order by quizid desc";
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    // add category for quiz
    public boolean addQuiz_Category(String categoryid, int quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "insert into category_Quiz (categoryid, quizid) values (?,?)";
            ps = conn.prepareStatement(query);
            ps.setString(1, categoryid);
            ps.setInt(2, quizid);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return false;
    }

    // check quiz exist bu quiz id
    public boolean checkQuizExist(String quizid) throws SQLException {
        String query = "select * from Quiz where quizid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public List<QuizUser> getAllQuizByCategoryId(String id) throws SQLException {
        String query = "select q.*, u.username from quiz q, category_Quiz c,[user] u\n"
                + "                where q.quizid = c.quizid and u.userid = q.userid \n"
                + "				and c.categoryid = ? order by q.quizid desc";
        List<QuizUser> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                QuizUser quiz = new QuizUser(rs.getInt("quizid"), rs.getInt("userid"), rs.getString("username"),
                        rs.getString("title"),
                        rs.getString("Description"), rs.getDate("Date_Created"), rs.getDate("Last_update"));
                list.add(quiz);
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String titleOfQuiz(int quizid) throws SQLException {
        String query = "select title from Quiz where quizid = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String titleQuiz = "";
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizid);
            rs = ps.executeQuery();
            if (rs.next()) {
                titleQuiz = rs.getString(1);
            }
            return titleQuiz;
        } catch (SQLException e) {
            return titleQuiz;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int getTotalNumberOfQuiz(int quizID) throws SQLException {
        String query = "select count(questionid) from Question where quizid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, quizID);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

    }

    // get total question of quiz
    public int getNumOfQuestionInQuiz(String quizid) throws SQLException {
        String query = "select count(*) from Question where quizid=?";
        List<Quiz> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, quizid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // count number of option
    public int getNumOfOptionInQues(String quizid, int questionid) throws SQLException {
        String query = "select count(*) from [Option] where quizid=? and questionid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, quizid);
            ps.setInt(2, questionid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

    }

    public Quiz getQuizDetails(String quizid) throws SQLException {
        String query = "select q.quizid,q.userid,q.title, q.[Description], q.Date_created, q.Last_update, e.questionid, e.question_title, e.Intruction, \n"
                + "                o.optionid, o.option_content, o.right_option, e.randomOption from Quiz q\n"
                + "                join Question e on q.quizid = e.quizid\n"
                + "                join [Option] o on o.quizid = q.quizid and\n"
                + "                o.questionid = e.questionid where q.quizid=?";

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        QuizDAO dao = new QuizDAO();

        try {
            Quiz quiz = new Quiz();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, quizid);
            rs = ps.executeQuery();
            int count = 0;
            int question = -1;

            while (rs.next()) {
                if (count == 0) {
                    quiz.setQuizid(rs.getInt(1));
                    quiz.setUserid(rs.getInt(2));
                    quiz.setTitle(rs.getString(3).trim());
                    quiz.setDescription(rs.getString(4).trim());
                    quiz.setDate_created(rs.getDate(5));
                    quiz.setLast_modified(rs.getDate(6));
                    count++;
                }
                if (rs.getInt(10) == 1) {
                    List<Question> list = quiz.getListQuestions();
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    question++;
                    list.add(new Question(rs.getInt(1), rs.getInt(7), rs.getString(8).trim(), rs.getString(9).trim(),
                            "innerQuestion" + (question), rs.getBoolean(13)));
                    quiz.setListQuestions(list);

                }
                List<Option> listO = quiz.getListQuestions().get(question).getListOptions();
                if (listO == null) {
                    listO = new ArrayList<>();
                }
                listO.add(new Option(rs.getInt(10), rs.getString(11).trim(), rs.getBoolean(12),
                        "que" + question + "_option" + rs.getInt(10)));
                quiz.getListQuestions().get(question).setListOptions(listO);
            }
            return quiz;

        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

    }

    public Question getQuestionDetails(String quizid, String questionid) throws SQLException {
        String query = "select q.quizid, e.questionid, e.question_title, e.Intruction,\n"
                + "                o.optionid, o.option_content, o.right_option from Quiz q\n"
                + "                join Question e on q.quizid = e.quizid\n"
                + "                join [Option] o on o.quizid = q.quizid and o.questionid = e.questionid\n"
                + "                where q.quizid=? and o.questionid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        QuizDAO dao = new QuizDAO();
        try {
            Question question = new Question();
            List<Option> listO = new ArrayList<>();
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, quizid);
            ps.setString(2, questionid);
            rs = ps.executeQuery();
            int count = 0;
            int option = -1;
            while (rs.next()) {
                if (count == 0) {
                    question.setQuizid(rs.getInt(1));
                    question.setQuestionid(rs.getInt(2));
                    question.setTitle(rs.getString(3));
                    question.setIntruction(rs.getString(4));
                    count++;
                }
                listO.add(new Option(rs.getInt(5), rs.getString(6), rs.getBoolean(7)));
                option++;
                question.setListOptions(listO);
            }
            return question;

        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

    }

    // get category of quiz
    public List<Category> getCategoryOfQuiz(String quizid) throws SQLException {

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            int n = 0;
            String query = "select * from Category";
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            List<Category> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1), rs.getString(2)));
            }
            String query2 = "select categoryid from category_Quiz where quizid=?";
            ps = conn.prepareStatement(query2);
            ps.setString(1, quizid);
            rs = ps.executeQuery();
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setCateOfQuiz(false);
            }
            while (rs.next()) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getCategoryId() == rs.getInt(1)) {
                        list.get(i).setCateOfQuiz(true);
                    }
                }
            }
            return list;
        } catch (SQLException e) {
            return null;
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // delete quiz by quizid
    public boolean deleteQuiz(String quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = " delete from Rate where quizid=?\n"
                    + "delete from [View] where quizid=?\n"
                    + "delete [Option] where quizid=?\n"
                    + "delete Question where quizid=?\n"
                    + "delete category_Quiz where quizid=?\n"
                    + "delete from Option_test\n"
                    + "where testid in (select quizid from Test where quizid=?)\n"
                    + "delete from Question_test\n"
                    + "where testid in (select quizid from Test where quizid=?)\n"
                    + "delete from Test\n"
                    + "where testid in (select quizid from Test where quizid=?)\n"
                    + "delete Quiz where quizid = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, quizid);
            ps.setString(2, quizid);
            ps.setString(3, quizid);
            ps.setString(4, quizid);
            ps.setString(5, quizid);
            ps.setString(6, quizid);
            ps.setString(7, quizid);
            ps.setString(8, quizid);
            ps.setString(9, quizid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // delete quiz for Update by quizid
    public boolean deleteQuizDetailForEditQuiz(String quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "delete [Option] where quizid=?\n"
                    + "delete Question where quizid=?\n"
                    + "delete category_Quiz where quizid=?\n";
            ps = conn.prepareStatement(query);
            ps.setString(1, quizid);
            ps.setString(2, quizid);
            ps.setString(3, quizid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // update time update of quiz by quizid
    public boolean updateQuizInQuizTable(String quizid, String title, String description) throws SQLException {
        String query = "update Quiz set Last_update= GETDATE()\n"
                + ", title = ? , [Description]=? where quizid=?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, title);
            ps.setString(2, description);
            ps.setString(3, quizid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // update view table
    public boolean updateNewViewQuiz(int userid, int quizid) throws SQLException {
        String query = "insert into [view] (userid, quizid, [date])\n"
                + "values (?,?,GETDATE())";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setInt(1, userid);
            ps.setInt(2, quizid);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    // add quiz viewed
    public boolean addQuizView(int viewer_id, int quizid) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = new DBContext().connection;
            String query = "INSERT INTO [dbo].[View]\n"
                    + "           ([userid]\n"
                    + "           ,[quizid]\n"
                    + "           ,[date])\n"
                    + "     VALUES\n"
                    + "           (?, ?, getDate())";
            ps = conn.prepareStatement(query);
            ps.setInt(1, viewer_id);
            ps.setInt(2, quizid);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    /////////Quiz manager////////////
    // get list num of quiz and category name name by category name
    public List<Category> getListNumOfQuizByCategory() throws SQLException {
        String query = "Select count(*) as 'Total of quiz', category_name\n"
                + "From Category as c, category_Quiz as cq\n"
                + "Where c.categoryid = cq.categoryid\n"
                + "Group By category_name";
        List<Category> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category cate = new Category(rs.getString(2), rs.getInt(1));
                list.add(cate);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    // number of viewers of each Quiz
    // count all quiz of search
    public List<Quiz_Viewer> getListNumOfViewersOfQuiz() throws SQLException {
        String query = "(Select u.userid, u.email, q.quizid, q.title, count(*) as 'count'\n"
                + "From Quiz as q join [View]as v\n"
                + "on q.quizid = v.quizid\n"
                + "join [User] as u\n"
                + "on u.userid = q.userid\n"
                + "Group By q.quizid, q.title, u.email, u.userid\n"
                + ")\n"
                + "union\n"
                + "(Select u.userid, u.email, q.quizid, q.title, 'count' = 0\n"
                + "From Quiz as q join [User] as u\n"
                + "on q.userid = u.userid\n"
                + "except\n"
                + "Select u.userid, u.email, q.quizid, q.title, 'count' = 0\n"
                + "From Quiz as q join [View]as v\n"
                + "on q.quizid = v.quizid\n"
                + "join [User] as u\n"
                + "on u.userid = q.userid\n"
                + "Group By q.quizid, q.title, u.email, u.userid)\n"
                + "order by 'count' DESC";
        List<Quiz_Viewer> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Quiz_Viewer qv = new Quiz_Viewer(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5));
                list.add(qv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public List<Quiz_Viewer> searchQuizbyName(String keywords) throws SQLException {
        String query = "select u.userid, u.email, q.quizid,q.title, count(v.userid) as numberOfView from [User] u\n"
                + "left join [Quiz] q on u.userid = q.userid left join [view] v \n"
                + "on q.quizid = v.quizid group by q.quizid,q.title, u.userid, u.email\n"
                + "having q.title like ? order by numberOfView desc";
        List<Quiz_Viewer> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + keywords + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Quiz_Viewer qv = new Quiz_Viewer(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5));
                list.add(qv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public List<Quiz_Viewer> getListQuizHaveNotViewed() throws SQLException {
        String query = "Select u.userid, u.email, q.quizid, q.title\n"
                + "From Quiz as q join [User] as u\n"
                + "on q.userid = u.userid\n"
                + "except\n"
                + "Select u.userid, u.email, q.quizid, q.title\n"
                + "From Quiz as q join [View]as v\n"
                + "on q.quizid = v.quizid\n"
                + "join [User] as u\n"
                + "on u.userid = q.userid";
        List<Quiz_Viewer> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Quiz_Viewer qv = new Quiz_Viewer(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), 0);
                list.add(qv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public int countAllQuiz() {
        String query = "select count(*) from Quiz";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().connection;
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static void main(String[] args) throws SQLException {
        QuizDAO q = new QuizDAO();
        List list = q.getAllQuiz(1);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
        System.out.println(q.getTotalOwnQuiz(1, ""));

        List list2 = q.quizPagination(1, 1);
        for (int i = 0; i < list2.size(); i++) {
            System.out.println(list2.get(i).toString());
        }
        System.out.println("=====check search=====");
        List list3 = q.searchQuizInLibrary(1, "ja");
        for (int i = 0; i < list3.size(); i++) {
            System.out.println(list3.get(i).toString());
        }

        System.out.println("=====check get title by quizid=====");
        System.out.println(q.getTitleByQuizId(3));
        // List list = q.getAllQuiz(1);
        // for (int i = 0; i < list.size(); i++) {
        // System.out.println(list.get(i).toString());
        // }
        // System.out.println(q.getTotalOwnQuiz(1));
        //
        // List list2 = q.quizPagination(1, 1);
        // for (int i = 0; i < list2.size(); i++) {
        // System.out.println(list2.get(i).toString());
        // }
        // System.out.println("=====check search=====");
        // List list3 = q.searchQuizInLibrary(1, "ja");
        // for (int i = 0; i < list3.size(); i++) {
        // System.out.println(list3.get(i).toString());
        // }
        // System.out.println("=====check view quiz detail=====");
        // List list4 = q.viewQuizDetail(6);
        // for (int i = 0; i < list4.size(); i++) {
        // System.out.println(list4.get(i).toString());
        // }
        Question question = q.getQuestionDetails("1", "2");
        System.out.println(question.toString());
        System.out.println(q.getUsernameByUserid(1));
        System.out.println("--------------------------");
        System.out.println(q.getTotalOwnQuiz(2, ""));
        System.out.println(q.quizPagination(1, 2).size());
        System.out.println("-------------------");
        System.out.println(q.getAllQuizByPage("1", "te").get(0));
        List list4 = q.searchQuizbyName("Test");
        for (int i = 0; i < list4.size(); i++) {
            System.out.println(list4.get(i).toString());
        }

        System.out.println("===================");
        System.out.println(q.getTotalOwnQuiz(3, ""));
    }

}
