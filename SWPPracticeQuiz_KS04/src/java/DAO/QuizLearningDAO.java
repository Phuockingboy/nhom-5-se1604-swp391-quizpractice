
package DAO;

import Model.Option;
import Model.Question;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class QuizLearningDAO {
    
    public Question radomOptionOfQuestion (Question question){
        Question nquestion = new Question();
        nquestion.setQuizid(question.getQuestionid());
        nquestion.setTitle(question.getTitle());
        nquestion.setIntruction(question.getIntruction());
        nquestion.setRandomQuestion(question.isRandomQuestion());
        List<Option> list = new ArrayList<>();
        
        if(question.getListOptions() != null && question.getListOptions().size()>0){
            QuizLearningDAO dao = new QuizLearningDAO();
            int [] randomOption = dao.randomListNumber(question.getListOptions().size());
            for (int i = 0; i < question.getListOptions().size(); i++) {
                list.add(question.getListOptions().get(randomOption[i]));
            }
        }
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setOptionid(i+1);
//            System.out.println(list.get(i).getOptionid());
        }
        nquestion.setListOptions(list);
        return nquestion;
    }
    
    // count the number of right option in a question
    public int countRightOption(Question question){
        int count = 0;
        for (int i = 0; i < question.getListOptions().size(); i++) {
            if(question.getListOptions().get(i).isRight_option()){
                count++;
            }
        }
        return count;
    }
    
    // random a list of number from 1 to n
    public int[] randomListNumber(int n){
        Random rd = new Random();
        int []listnumber = new int[n];
        for (int i = 0; i < n; i++) {
            listnumber[i]= i+1;
        }
        for (int i = 0; i < 3; i++) {
            int n1 = rd.nextInt(n);
            int n2 = rd.nextInt(n);
            int temp = listnumber[n1];
            listnumber[n1] = listnumber[n2];
            listnumber[n2] = temp;
        }
        return listnumber;
    }
    public static void main(String[] args) {
        QuizLearningDAO dao = new QuizLearningDAO();
        int []i = dao.randomListNumber(4);
        for (int j = 0; j < i.length; j++) {
            System.out.println(i[j]);
        }
        Question q = new Question();
        System.out.println(dao.radomOptionOfQuestion(q));
        
    }
}
