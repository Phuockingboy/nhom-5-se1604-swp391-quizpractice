<%-- 
    Document   : test
    Created on : Jul 12, 2022, 3:54:29 PM
    Author     : fptshop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            .modal-header, .close {

                color:white !important;
                text-align: center;
                font-size: 30px;
            }
            h4 {

                color:black;
                text-align: center;
                font-size: 30px;
            }
            .feedback{
                width: 100%;
                max-width: 780px;
                background: rgba(0, 0, 0, 0.65);
                margin: 0 auto;
                padding: 15px;
                box-shadow: 1px 1px 16px rgba(0, 0, 0, 0.3);
            }
            .survey-hr{
                margin:10px 0;
                border: .5px solid #ddd;
            }
            .star-rating {
                margin: 25px 0 0px;
                font-size: 0;
                white-space: nowrap;
                display: inline-block;
                width: 175px;
                height: 35px;
                overflow: hidden;
                position: relative;
                background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
            }
            .star-rating i {
                opacity: 0;
                position: absolute;
                left: 0;
                top: 0;
                height: 100%;
                width: 20%;
                z-index: 1;
                background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
            }
            .star-rating input {
                -moz-appearance: none;
                -webkit-appearance: none;
                opacity: 0;
                display: inline-block;
                width: 20%;
                height: 100%;
                margin: 0;
                padding: 0;
                z-index: 2;
                position: relative;
            }
            .star-rating input:hover + i,
            .star-rating input:checked + i {
                opacity: 1;
            }
            .star-rating i ~ i {
                width: 40%;
            }
            .star-rating i ~ i ~ i {
                width: 60%;
            }
            .star-rating i ~ i ~ i ~ i {
                width: 80%;
            }
            .star-rating i ~ i ~ i ~ i ~ i {
                width: 100%;
            }
            .choice {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                text-align: center;
                padding: 20px;
                display: block;
            }
            span.scale-rating{
                margin: 5px 0 15px;
                display: inline-block;

                width: 100%;

            }
            span.scale-rating>label {
                position:relative;
                -webkit-appearance: none;
                outline:0 !important;
                border: 1px solid grey;
                height:33px;
                margin: 0 5px 0 0;
                width: calc(10% - 7px);
                float: left;
                cursor:pointer;
            }
            span.scale-rating label {
                position:relative;
                -webkit-appearance: none;
                outline:0 !important;
                height:33px;

                margin: 0 5px 0 0;
                width: calc(10% - 7px);
                float: left;
                cursor:pointer;
            }
            span.scale-rating input[type=radio] {
                position:absolute;
                -webkit-appearance: none;
                opacity:0;
                outline:0 !important;
                /*border-right: 1px solid grey;*/
                height:33px;

                margin: 0 5px 0 0;

                width: 100%;
                float: left;
                cursor:pointer;
                z-index:3;
            }
            span.scale-rating label:hover{
                background:#fddf8d;
            }
            span.scale-rating input[type=radio]:last-child{
                border-right:0;
            }
            span.scale-rating label input[type=radio]:checked ~ label{
                -webkit-appearance: none;

                margin: 0;
                background:#fddf8d;
            }
            span.scale-rating label:before
            {
                content:attr(value);
                top: 7px;
                width: 100%;
                position: absolute;
                left: 0;
                right: 0;
                text-align: center;
                vertical-align: middle;
                z-index:2;
            }
        </style>
    </head>
    <body>

        <div class="container">
            <h2>Modal Login Example</h2>
            <!-- Trigger the modal with a button -->
            <button type="button" id="myBtn1">Login</button>

            <!-- Modal -->
            <div class="modal fade" id="myModal1" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="padding:35px 50px;">
                            <h4><span class=""></span>Edit rate and feedback for this Quiz</h4>
                        </div>
                        <div class="modal-body" style="padding:40px 50px;">
                            <form method="Post" action="RateAndFeedbackController">
                                <span style="color: black; margin-left: 22px">1. Your overall experience with us ?</span><br>
                                <span style="margin-left: 22px" class="">
                                    <input type="radio" name="gender" value="male" /> Male
                                    <input type="radio" name="gender" value="female" checked="checked" /> Female
                                    <input hidden type="text" name="quizid" value=${requestScope.quiz.getQuizid()}>
                                </span>
                                <div class="clear"></div>
                                <br>
                                <div style="width: 600px;" class="">
                                    <span style="color: black; margin-left: 22px" for="exampleFormControlTextarea1" class="form-label">2. Write your feedback</span>
                                    <textarea name="feedback" style="height: 150px; width: 450px; margin-top:18px; margin-left: 22px"  class="form-control" ></textarea>
                                </div>
                                <br>
                                <div class="clear"></div>
                                <input style="background:#367ced;color:#fff;padding:12px;border:0; border-radius: 7px; margin-left: 20px; font-weight: 700" type="submit"
                                       value="Save changes">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div style="float:right">
                            </div>
                        </div>
                    </div>

                </div>
            </div> 
        </div>

        <script>
            $(document).ready(function () {
                $("#myBtn1").click(function () {
                    $("#myModal1").modal();
                });
            });
        </script>

    </body>
</html>