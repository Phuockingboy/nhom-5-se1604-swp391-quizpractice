<%-- 
    Document   : UserAdmin
    Created on : Jul 21, 2022, 12:07:18 AM
    Author     : dangm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>Admin Dashboard</title>
        <style>
            li{
                display: inline;
            }
            td,th,tr{
                border: 1px solid lightgray;
            }
            .search{
                margin-top: 37px;
            }
            .search-button{
                margin-left: 20px;
                border-radius: 5px;
                background-color: #0a6bff;
                color: white;
                font-size: 20px;
                height: 35px;
            }
            .search-button:hover{
                background-color: #065dd8;
            }
            .modal-header, h4, .close {
                background-color: #5cb85c;
                color:white !important;
                text-align: center;
                font-size: 30px;
            }
            .modal-footer {
                background-color: #f9f9f9;
            }
        </style>
        <script  src = "./js/chart.min.js" ></script>
    </head>

    <!--  `body` tag options:
        Apply one or more of the following classes to to the body tag
        to get the desired effect
        * sidebar-collapse
        * sidebar-mini-->

    <body id="page-top" class="home">
        <div id="page-wrap">
            <jsp:include page="AdminHeader.jsp"></jsp:include>
                <div  class="hold-transition sidebar-mini"
                      style="
                      display: flex;
                      height: auto;
                      justify-content: center;
                      background-color: #f4f6f9;
                      "
                      >
                    <div class="wrapper" style="width: 80%; " >
                        <!--Content Wrapper. Contains page content--> 
                        <div class="content-wrapper" style="margin: 0">
                            <!--Content Header (Page header)--> 
                            <div class="content-header">
                                <div class="container-fluid">
                                    <div class="row mb-2">
                                        <div class="col-sm-6">
                                            <h1 class="m-0" style="color: gold">Admin Dashboard</h1>
                                        </div>
                                        <div class="col-sm-6">
                                            <form action="UserAdminController" method="POST">
                                                <div class="search">
                                                    <input style="border:2px solid black;
                                                           border-radius: 5px;padding-left: 5px; font-size: 20px; height: 35px; font-weight: bold; " 
                                                           type="Text" name="Search" value="" placeholder="Enter user name">
                                                    <span><button class= "search-button" type="submit" name="btn-search" class="">Search</button></span>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-6"> 
                                            <div><h3 style="color: green;">${message}</h3></div>
                                            <div class="card" style="font-size: 20px;">
                                                <div class="card-header border-0">
                                                    <h3 class="card-title" style="font-size: 20px; font-weight: bold;">User</h3>
                                                    <div class="card-tools"></div>
                                                </div>
                                                <div class="card-body">
                                                <c:if test="${listUser==null ||listUser.size()==0 }">
                                                    <h1>Can't find any person.</h1>
                                                </c:if>
                                                <c:if test="${listUser!=null  && listUser.size()>0 }">
                                                    <table class="table table-dark table-striped">
                                                        <thead style="background-color: #2095c1">
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Quiz Created</th>
                                                                <th>Public Profile</th>
                                                                <th>Ban user</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="background-color: lightblue">

                                                            <c:forEach items="${listUser}" var="user">
                                                                <tr>
                                                                    <td style="max-width: 120px">${user.getUserName()}</td>
                                                                    <td>${user.countQuizCreated}</td>
                                                                    <td><a href="ViewOtherQuizController?userid=${user.getId()}" class="">
                                                                            <i class="fas fa-search"></i>
                                                                            Detail
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <c:if test="${user.getRoleId() == 0}">
                                                                            <a href="#banmodal" class="buton" data-id="${user.getId()}" data-toggle="modal" data-target="#banmodal-${user.getId()}">
                                                                                Ban
                                                                            </a>
                                                                        </c:if>
                                                                        <c:if test="${user.getRoleId() == 10}">
                                                                            <a href="#unbanmodal" class="buton" data-id="${user.getId()}" data-toggle="modal" data-target="#unbanmodal-${user.getId()}">
                                                                                Unban
                                                                            </a>  
                                                                        </c:if>
                                                                        <div class="modal fade" role="dialog" id="banmodal-${user.getId()}">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header" style="background-color:#1270a6 ;padding: 25px 40px;">
                                                                                            <h2 class="modal-title">Warning</h2>
                                                                                        </div>
                                                                                        <div class="modal-body" style="padding: 25px 40px;" >
                                                                                            <p style="font-size: 30px;">Do you want to ban this user?</p>
                                                                                        </div>
                                                                                        <div class="modal-footer" style="padding: 25px 40px;">
                                                                                            <form id="requestacallform" action="UserAdminController?Action=Ban" method="POST" name="">
                                                                                                <input type="text" value="${user.getId()}" name="userId" id="opId" hidden >
                                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: lightgray; padding: 10px 20px;">Cancel</button>
                                                                                                <button type="submit" class="btn btn-success" style="background-color:#1270a6; padding: 10px 20px;">Submit</button>
                                                                                            </form>                                    
                                                                                        </div>          
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal fade" role="dialog" id="unbanmodal-${user.getId()}">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header" style="background-color:#1270a6 ;padding: 25px 40px;">
                                                                                            <h2 class="modal-title">Warning</h2>
                                                                                        </div>
                                                                                        <div class="modal-body" style="padding: 25px 40px;" >
                                                                                            <p style="font-size: 30px;">Do you want to unban this user?</p>
                                                                                        </div>
                                                                                        <div class="modal-footer" style="padding: 25px 40px;">
                                                                                            <form id="requestacallform" action="UserAdminController?Action=Unban" method="POST" name="">
                                                                                                <input type="text" value="${user.getId()}" name="userId" id="opId" hidden >
                                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: lightgray; padding: 10px 20px;">Cancel</button>
                                                                                                <button type="submit" class="btn btn-success" style="background-color:#1270a6; padding: 10px 20px;">Submit</button>
                                                                                            </form>                                    
                                                                                        </div>          
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>

                                                        </tbody>
                                                    </table>
                                                </c:if>
                                            </div>
                                        </div>
                                        <!--/.card--> 
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="card" style="font-size: 20px;">
                                            <div class="card-header border-0">
                                                <h3 class="card-title" style="font-size: 20px; font-weight: bold;">QuizPractice OverView</h3>
                                                <div class="card-tools"></div>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-dark table-striped">
                                                    <thead style="background-color: #2388b4">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th> Information</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="background-color: lightblue">
                                                        <tr>
                                                            <td><i class="fas fa-book-open" style="font-size:30px;color:black;"></i></td>
                                                            <td>${numQuiz}-Quiz</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="icon md-user-minus" style="font-size: 30px;"></i></td>
                                                            <td>${numUser}-User</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="fas fa-th" style="font-size:30px;color:black;"></i></td>
                                                            <td>${numCategory}-Category</td>
                                                        </tr>
                                                        <tr>
                                                            <td><i class="icon md-users" style="font-size: 30px;"></i></td>
                                                            <td>${numClass}-Class</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--/.card-->
                                        <div class="card" style="font-size: 20px;">
                                            <div class="card-header border-0">
                                                <h3 class="card-title" style="font-size: 20px; font-weight: bold;">Number of visitors</h3>
                                                <div class="card-tools"></div>
                                            </div>
                                            <div>
                                                <span>Total visitor this week: </span>
                                                <span id="totalVisitorThisWeek"></span>
                                                <span>people.</span>
                                            </div>
                                            <div class="card-body">
                                                <canvas id="myChart" style="font-size: 50px;"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/.col-md-6--> 
                            </div>
                            <!--/.row--> 
                        </div>
                        <!--/.container-fluid--> 
                    </div>
                    <!--/.content--> 
                    <input id="visitorThisWeek" value="${thisweekVisit}" hidden>
                    <input id="visitorLastWeek" value="${lastweekVisit}" hidden>
                </div>
                <!--/.content-wrapper--> 

                <!--Control Sidebar--> 
                <aside class="control-sidebar control-sidebar-dark">
                    <!--Control sidebar content goes here--> 
                </aside>
                <!--/.control-sidebar--> 
            </div>
        </div>
        <script>
            let thisweek = document.getElementById("visitorThisWeek").value;
            const day = thisweek.split(",");
            var tw0 = day[0];
            var tw1 = day[1];
            var tw2 = day[2];
            var tw3 = day[3];
            var tw4 = day[4];
            var tw5 = day[5];
            var tw6 = day[6];
            var totalThisWeek = parseInt(tw0, 10) + parseInt(tw1, 10) + parseInt(tw2, 10) + parseInt(tw3, 10) + parseInt(tw4, 10) + parseInt(tw5, 10) + parseInt(tw6, 10);
            document.getElementById("totalVisitorThisWeek").innerHTML = totalThisWeek;
            let lastweek = document.getElementById("visitorLastWeek").value;
            const day1 = lastweek.split(",");
            var lw0 = day1[0];
            var lw1 = day1[1];
            var lw2 = day1[2];
            var lw3 = day1[3];
            var lw4 = day1[4];
            var lw5 = day1[5];
            var lw6 = day1[6];
            const ctx = document.getElementById('myChart').getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Monday', 'Tueday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    datasets: [{
                            label: 'Last week',
//                            data: [lw0, lw1, lw2, lw3, lw4, lw5, lw6],
                            data: [1, 1, 2, 3, 4, 5, 6],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
//                                'rgba(54, 162, 235, 0.2)',
//                                'rgba(255, 206, 86, 0.2)',
//                                'rgba(75, 192, 192, 0.2)',
//                                'rgba(153, 102, 255, 0.2)',
//                                'rgba(255, 159, 64, 0.2)'
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(255, 99, 132, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
//                                'rgba(54, 162, 235, 1)',
//                                'rgba(255, 206, 86, 1)',
//                                'rgba(75, 192, 192, 1)',
//                                'rgba(153, 102, 255, 1)',
//                                'rgba(255, 159, 64, 1)'
                                'rgba(255, 99, 132, 1)',
                                'rgba(255, 99, 132, 1)',
                                'rgba(255, 99, 132, 1)',
                                'rgba(255, 99, 132, 1)',
                                'rgba(255, 99, 132, 1)',
                                'rgba(255, 99, 132, 1)'
                            ],
                            borderWidth: 1
                        }, {
                            label: 'This week',
                            data: [tw0, tw1, tw2, tw3, tw4, tw5, tw6],
                            backgroundColor: [
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)'
                            ],
                            borderColor: [
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            $(document).on("click", ".buton", function () {
                var myId = $(this).data('id');
                $(".modal-footer #opId").val(myId);
            });
        </script>
    </body>
    <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
    <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
    <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
</html>
