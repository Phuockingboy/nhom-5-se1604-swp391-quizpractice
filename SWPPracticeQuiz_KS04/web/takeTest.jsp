<%-- 
    Document   : Test
    Created on : Jun 21, 2022, 4:20:31 PM
    Author     : dangm
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from tk-themes.net/html-megacourse/quizz-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 May 2022 16:35:11 GMT -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="jquery.min.js"></script>
      
        <title>Mega Course - Learning and Courses HTML5 Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

        <script type="text/javascript" src="js/script.js"></script>
        <style>
            .modal-header{
                background-color: #5bc0de;
                color:white !important;
                text-align: center;
                font-size: 30px;
            }
            #mh1 {
                background-color: white;
                color:white !important;
                text-align: center;
                font-size: 30px;
            }
            #h41 {

                color:#F70a0a !important;
                text-align: center;
                font-size: 30px;
            }
            #formTimeOut{
                margin-top: 400px;
            }

        </style>


    </head>
    <body id="page-top">

        <!-- PAGE WRAP -->
        <div id="page-wrap">

            <div class="top-nav">
                <p></p>
                <h4 class="sm black bold"><a href="HomeController" style="color: #0f649d">Home</a>/take test</h4>

                <ul class="top-nav-list">
                    <!--                    <li class="prev-course"><a href="#"><i class="icon md-angle-left"></i><span class="tooltip">Prev</span></a></li>
                                        <li class="next-course"><a href="#"><i class="icon md-angle-right"></i><span class="tooltip">Next</span></a></li>-->
                    <li class="backpage">
                        <a href="ViewQuizDetailController?quizid=${test.quizid}"><i class="icon md-close-1"></i></a>
                    </li>
                </ul>

            </div>
            <section id="quizz-intro-section" class="quizz-intro-section learn-section">
                <div class="container">

                    <div class="title-ct">
                        <h3><strong>Test: </strong>${test.title}</h3>
                        <div class="tt-right">
                            <c:if test="${questionTest.questionid!=test.listQuestionTest.size()}">
                                <a onclick="submitForm(${questionTest.questionid+1})" class="skip"><i class="icon md-arrow-right"></i>Skip quizz</a>
                            </c:if>
                        </div>
                    </div>
                    <!--                        <form action="TakeTestController">
                                                <input type="text" name="testid" value="${testid}">-->
                    <div class="question-content-wrap">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="question-content">
                                    <h4 class="sm">Question ${questionTest.questionid} - ${questionTest.questionTitle}</h4>
                                    <p>Choose ${questionTest.numberOfRightAnswer} answer.</p>
                                    <div class="answer">
                                        <h4 class="sm">Answer</h4>

                                        <form id="my_form" action="TakeTestController" method="Post">
                                            <input type="hidden" name="minute" id="minute">
                                            <input type="hidden" name="questionid" id="getQuestionID">
                                            <input type="hidden" name="current_questionid" id="current_questionid" value="${questionTest.questionid}">
                                            <input type="hidden" name="submitAllAnser" id="submitAllAnser" value="">
                                            <input type="hidden" name="testid" value="${testid}">
                                            <ul class="answer-list">
                                                <c:forEach items="${questionTest.list_optionTest}" var="o">
                                                    <li>
                                                        <input<c:if test="${o.optionStatus==true}"> checked="" </c:if> type="checkbox" name="Question${questionTest.questionid}" id="option${o.optionid}" value="${o.optionid}" >
                                                        <label for="option${o.optionid}">
                                                            <i class="icon icon_check md-check-1"></i>
                                                            ${requestScope.s.charAt(o.optionid-1)}.      ${o.optionContent}
                                                        </label>
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </form>

                                    </div>
                                    <c:if test="${questionTest.questionid!=1}">
                                        <a onclick="submitForm(${questionTest.questionid-1})" class="mc-btn btn-style-6">Previous Question</a>
                                    </c:if>
                                    <c:if test="${questionTest.questionid!=test.listQuestionTest.size()}">
                                        <a onclick="submitForm(${questionTest.questionid+1})" class="mc-btn btn-style-6">Next question</a>
                                    </c:if>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <aside class="question-sidebar">
                                    <div class="score-sb">
                                        <h4 class="title-sb sm bold sidebar-heading" ><div>Time <span id="time">${minutee}</span> </div></h4>
                                        <ul>
                                            <c:forEach items="${test.listQuestionTest}" var="o"> 
                                                <li <c:if test="${o.done == true && o.questionid != requestScope.questionid}"> class="val"</c:if>
                                                    <c:if test="${o.questionid == requestScope.questionid}"> class="active"</c:if>                             ><a onclick="submitForm(${o.questionid})"><i class="icon"></i>Question ${o.questionid}</a> </li>
                                                </c:forEach>
                                        </ul>
                                    </div>
                                    <!--<a class="submit mc-btn btn-style-1 btn btn-default btn-lg" id="finishTest"> Submit all answer</a>-->

                                    <input type="submit" value="Submit all answer" class="submit mc-btn btn-style-1" id="finishTest" name="submitAllAnswer">
                                </aside>
                            </div>
                        </div>
                    </div>

                    <!--</form>-->
                    <!-- Modal finish test -->
                    <!--<form>-->
                    <div class="modal fade" id="finishTestModal" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header" style="padding:35px 50px;">
                                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                    <h2><span class="glyphicon glyphicon-exclamation-sign"></span> Do you want to submit your test?</h2>
                                </div>
                                <div class="modal-body" style="padding:40px 50px;">
                                    <h1>Remember to check all your work before submitting.</h1>
                                </div>
                                <div class="modal-footer">
                                    <div style="float:right">
                                        <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">No</button>
                                        <button onclick="submitFormAll(${questionTest.questionid})" type="submit" class="btn btn-primary btn-lg">Yes</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--</form>-->
                    <!-- Modal time out -->
                    <div id="formTimeOut">
                        <!--<form >-->
                        <div class="modal fade" id="timeOutSubmit" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div id="mh1" class="modal-header" style="padding:35px 50px;">
                                        <h4 id="h41"><span class="glyphicon glyphicon-info-sign"></span> Time out!</h4>
                                    </div>
                                    <div class="modal-body" style="padding:40px 50px;">
                                        <h1>You're late.</h1>
                                    </div>
                                    <div class="modal-footer">
                                        <div style="float:right">
                                            <button onclick="submitFormAll(${questionTest.questionid})" type="submit" class="btn btn-primary btn-lg">ok</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </div>
            </section>


        </div>
        <!-- END / PAGE WRAP -->
        <script>
            $(document).ready(function () {
                $("#finishTest").click(function () {
                    $("#finishTestModal").modal();
                });
            });
            function submitForm(questionid) {
                //console.log(1);
                var time = document.getElementById('time').textContent;
                document.getElementById('minute').value = time;
                document.getElementById('getQuestionID').value = questionid;
                document.getElementById('my_form').submit();
            }
            ;
            function submitFormAll(questionid) {
                //console.log(1);
                var time = document.getElementById('time').textContent;
                document.getElementById('minute').value = time;
                document.getElementById('getQuestionID').value = questionid;
                document.getElementById('submitAllAnser').value = 'submitAllAnser';
                document.getElementById('my_form').submit();
            }
            ;

            function startTimer(duration, display) {
//                var questionid = document.getElementById('current_questionid').value;
                var timer = duration, minutes, seconds;
                setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    display.textContent = minutes + ":" + seconds;
                    if (--timer < 0) {
//                        timer = duration;
//                        function () {
//                            $('#timeOutSubmit').modal();
//                        }
//                        ;
                        setTimeout(function () {
                            $('#timeOutSubmit').modal();
                        }, 0);
                        display.textContent = 00 + ":" + 00;
                    }
//                    if(--timer === 0){
//                        submitFormAll(questionid);
//                    }
                }, 1000);
            }

            window.onload = function () {
                var time = 60 * ${minute},
                        display = document.querySelector('#time');
                startTimer(time, display);
            };
//            var TimeTest = document.getElementById("time").value;
//            function nextQuestion(questionid, testid) {
//
//            }


        </script>
        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="assets/vendor/jquery.countdown.min.js"></script>

        <!-- Init -->
        <script src="assets/js/countdown.js"></script>
    </body>

</html>
