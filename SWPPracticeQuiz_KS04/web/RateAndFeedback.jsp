
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Mega Course - Learning and Courses HTML5 Template</title>
        <style>
            .instruction{
                display: none;
                color: green;
                font-weight: bold;
            }
            .feedback{
                width: 100%;
                max-width: 780px;
                background: rgba(0, 0, 0, 0.65);
                margin: 0 auto;
                padding: 15px;
                box-shadow: 1px 1px 16px rgba(0, 0, 0, 0.3);
            }
            .survey-hr{
                margin:10px 0;
                border: .5px solid #ddd;
            }
            .star-rating {
                margin: 25px 0 0px;
                font-size: 0;
                white-space: nowrap;
                display: inline-block;
                width: 175px;
                height: 35px;
                overflow: hidden;
                position: relative;
                background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
            }
            .star-rating i {
                opacity: 0;
                position: absolute;
                left: 0;
                top: 0;
                height: 100%;
                width: 20%;
                z-index: 1;
                background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
            }
            .star-rating input {
                -moz-appearance: none;
                -webkit-appearance: none;
                opacity: 0;
                display: inline-block;
                width: 20%;
                height: 100%;
                margin: 0;
                padding: 0;
                z-index: 2;
                position: relative;
            }
            .star-rating input:hover + i,
            .star-rating input:checked + i {
                opacity: 1;
            }
            .star-rating i ~ i {
                width: 40%;
            }
            .star-rating i ~ i ~ i {
                width: 60%;
            }
            .star-rating i ~ i ~ i ~ i {
                width: 80%;
            }
            .star-rating i ~ i ~ i ~ i ~ i {
                width: 100%;
            }
            .choice {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                text-align: center;
                padding: 20px;
                display: block;
            }
            span.scale-rating{
                margin: 5px 0 15px;
                display: inline-block;

                width: 100%;

            }
            span.scale-rating>label {
                position:relative;
                -webkit-appearance: none;
                outline:0 !important;
                border: 1px solid grey;
                height:33px;
                margin: 0 5px 0 0;
                width: calc(10% - 7px);
                float: left;
                cursor:pointer;
            }
            span.scale-rating label {
                position:relative;
                -webkit-appearance: none;
                outline:0 !important;
                height:33px;

                margin: 0 5px 0 0;
                width: calc(10% - 7px);
                float: left;
                cursor:pointer;
            }
            span.scale-rating input[type=radio] {
                position:absolute;
                -webkit-appearance: none;
                opacity:0;
                outline:0 !important;
                /*border-right: 1px solid grey;*/
                height:33px;

                margin: 0 5px 0 0;

                width: 100%;
                float: left;
                cursor:pointer;
                z-index:3;
            }
            span.scale-rating label:hover{
                background:#fddf8d;
            }
            span.scale-rating input[type=radio]:last-child{
                border-right:0;
            }
            span.scale-rating label input[type=radio]:checked ~ label{
                -webkit-appearance: none;

                margin: 0;
                background:#fddf8d;
            }
            span.scale-rating label:before
            {
                content:attr(value);
                top: 7px;
                width: 100%;
                position: absolute;
                left: 0;
                right: 0;
                text-align: center;
                vertical-align: middle;
                z-index:2;
            }

        </style>
    </head>
    <body id="page-top" class="home">

        <!-- PAGE WRAP -->
        <div id="page-wrap">


            <!-- HEADER -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- END / HEADER -->
                    <!-- LOGIN -->
                    <section id="login-content" class="login-content">
                        <div class="awe-parallax bg-login-content"></div>
                        <div class="awe-overlay"></div>
                            <div class="row">
                                <!-- FORM -->
                                <div class="">
                                        <section>
                                            <div class="rt-container">
                                                <div class="col-rt-12">
                                                    <div class="Scriptcontent">
                                                        <div  class="feedback" style="width: 800px; margin-top: 5px ">
                                                            <div style="text-align: center; color: white; font-weight: 700; font-size: 28px">Hi ${username}, please rate the Quiz</p>
                                                            <form method="get" action="ViewQuizDetailController?quizid=${quizid}">
                                                                <div style="color: white; margin-left: 22px;font-size: 22px">Your overall experience with this Quiz? ?</div>
                                                                <span style="margin-left: 22px" class="star-rating">
                                                                    <input type="radio" name="rating1" value="1"><i></i>
                                                                    <input type="radio" name="rating1" value="2"><i></i>
                                                                    <input type="radio" name="rating1" value="3"><i></i>
                                                                    <input type="radio" name="rating1" value="4"><i></i>
                                                                    <input type="radio" name="rating1" value="5"><i></i>
                                                                    
                                                                </span>
                                                                <div class="clear"></div>
                                                                <br>
                                                                <div style="width: 600px;" class="">
                                                                    <textarea placeholder="Write your feedback here..." name="feedback" style="height: 150px; width: 700px; margin-top:18px; margin-left: 22px"  class="form-control" ></textarea>
                                                                </div>
                                                                <br>
                                                                <div class="clear"></div>
                                                                <input style="background:#367ced;color:#fff;padding:12px;border:0; border-radius: 7px; margin-left: 18px; font-weight: 700;font-size: 22px" type="submit"
                                                                       value="Submit your review">
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                </div>
                                <!-- END / FORM -->

                            </div>
                    </section>
                    <!-- END / LOGIN -->

                <!-- FOOTER -->
            <jsp:include page="Footer.jsp"></jsp:include>
            <!-- END / FOOTER -->

        </div>
        <!-- END / PAGE WRAP -->

        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </body>

</html>
