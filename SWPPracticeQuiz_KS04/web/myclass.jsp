<%-- 
    Document   : viewOwnQuiz
    Created on : Jun 12, 2022, 4:23:50 PM
    Author     : fptshop
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <title>QuizPractice View Other Profile</title>
        <style>
            .search-button{
                margin-left: 20px;
                border-radius: 5px;
                background-color: #0a6bff;
                color: white;
                font-size: 20px;
                height: 35px;
            }
            .search-button:hover{
                background-color: #065dd8;
            }
            .box{
                box-shadow: 2px 2px #eee ;
                background-color: white; 
                border-radius: 5px; 
                margin-bottom: 20px; 
                padding: 10px 20px;
            }
            .box:hover{
                box-shadow: 2px 2px #eee ;
                background-color: #EFFFBF; 
            }
            .popup-with-zoom-anim{
                padding-left: 40px;
                background-color: inherit;
                color: white; 
                text-align: left;

            }
            .popup-with-zoom-anim:hover{
                background-color: #0C406F;
            }
            .aclass{
                display: flex;
                
            }
        </style>
    </head>
    <body id="page-top" class="home">

        <!-- PAGE WRAP -->
        <div id="page-wrap">
            <!-- HEADER -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- END / HEADER -->


                <!-- SUB BANNER -->
                <section class="sub-banner section">
                    <div class="awe-parallax bg-profile-feature"></div>
                    <div class="awe-overlay overlay-color-3"></div>
                    <div class="container">
                        <div class="info-author">
                            <div class="image">
                            <c:if test="${sessionScope.user.avatar==null}">
                                <img src="imageUpload/default-avatar.png" alt="avatar">
                            </c:if>
                            <c:if test="${sessionScope.user.avatar!=null}">
                                <img src="imageUpload/${sessionScope.user.avatar}" alt="avatar">
                            </c:if>
                        </div>    
                        <div class="name-author">
                            <h2 class="big">${sessionScope.user.userName}</h2>
                        </div>     
                        <div class="address-author">
                            <i style="color: white;"class="icon md-email"></i>
                            <h3>${sessionScope.user.email}</h3>
                        </div>
                        <div class="address-author" style="margin-left: 80px;">
                            <i style="color: white;" class="icon md-user-minus"></i>
                            <h3>${sessionScope.user.aboutMe}</h3>
                        </div>

                    </div>
                    <div class="info-follow">
                        <div class="trophies">
                            <span>${numberOfQuiz}</span>
                            <p>Created</p>
                        </div>
                        <div class="trophies">
                            <span>${sessionScope.user.phone}</span>
                            <p>Phone</p>
                        </div>
                        <div class="trophies">
                            <span>${countNumberOfClass}</span>
                            <p>Class</p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END / SUB BANNER -->


            <!-- PAGE CONTROL -->
            <section class="page-control">
                <div class="container">
                    <div class="page-info">
                        <a href="HomeController"><i class="icon md-arrow-left"></i>Back to home</a>
                    </div>
                    <div class="page-info" >
                        <a href="#" style="color:#A5A5A5"> <span>| My Classes</span></a>
                    </div>
                    <div class="page-view">
                        View
                        <span class="page-view-info view-grid" title="View grid"><i class="icon md-ico-2"></i></span>
                        <span class="page-view-info view-list  active" title="View list"><i class="icon md-ico-1"></i></span>
                    </div>
                </div>
            </section>
            <!-- END / PAGE CONTROL -->

            <section class="navbarr page-control">
                <div class="container" style="margin-left: 10%;">
                    <div class="page-info" >
                        <a href="ViewMyClassesController?task=viewAll" <c:if test="${requestScope.task=='viewAll'}">style="color: #A5A5A5;"</c:if>
                           <c:if test="${requestScope.task !='viewAll'}">class='link'</c:if>> All Your Classes</a>
                        </div>
                        <div class="page-info" >
                            <a href="ViewMyClassesController?task=viewCreated" <c:if test="${requestScope.task=='viewCreated'}">style="color: #A5A5A5;"</c:if>
                           <c:if test="${requestScope.task !='viewCreated'}">class='link'</c:if>>|  Created</a>
                        </div>
                        <div class="page-info" >
                            <a href="ViewMyClassesController?task=viewAdmin" <c:if test="${requestScope.task=='viewAdmin'}">style="color: #A5A5A5;"</c:if>
                           <c:if test="${requestScope.task !='viewAdmin'}">class='link'</c:if>>| Admin Of</a>
                        </div>
                    </div>
                </section>


                <!-- CATEGORIES CONTENT -->
                <section id="categories-content" class="categories-content">
                    <div class="container">
                        <div class="row" >
                            <div class="col-md-11 col-md-push-1">

                                <div class="content list">
                                    <div class="row">
                                        <div style="margin-top: 40px; margin-bottom: 20px; float: left; margin-left: 0px;" >
                                            <form action="ViewMyClassesController" method="POST">
                                                <input type="text" name="task" value="${requestScope.task}" hidden="">
                                                <input style="border:2px solid black;
                                                       border-radius: 5px;padding-left: 5px; font-size: 20px; height: 35px; font-weight: bold; " 
                                                       type="Text" name="txtSearch" value="${txtSearch}" placeholder="">
                                            <span><button class= "search-button" type="submit" name="btn-search" class="">Search</button></span>
                                        </form> 
                                    </div>
                                </div>
                                <!--CLASS-->
                                <div class="row" id="content" style="padding-top: 18px ;" >
                                    <!-- ITEM -->
                                    <c:forEach items="${listClass}" var = "o">
                                        <div class="row aclass" >
                                            <div class='box col-sm-8 col-md-8' onclick="gotoClass('goto${o.classid}')" >
                                                <form action="ViewQuizInClassController" method="post" id="goto${o.classid}">
                                                    <input name='classid' value="${o.classid}" hidden="">
                                                </form>

                                                <div>
                                                    <div class="meta-categories" style="margin-bottom: 20px; font-size: 25px; font-weight: bold;">${o.name}</div>
                                                    <p style='color: black;'>Created By: <a href="ViewOtherQuizController?userid=${o.createid}">${o.username}</a></p>
                                                </div>



                                            </div>
                                            <div class="col-sm-2 col-md-2">
                                                <c:if test="${o.createid == sessionScope.user.id}">
                                                <div class="add-section">
                                                    <a href="#deleteClass${o.classid}" class="mc-btn-3 btn-style-1 popup-with-zoom-anim" style="color: black; padding-left: 10px;margin-left: 0;">Delete Class</a>
                                                    <div id="deleteClass${o.classid}" class="design-course-popup pp-add-section zoom-anim-dialog mfp-hide">
                                                        <label for="" style="font-size: 40px; font-weight: bold; margin-bottom: 30px;">Are you sure to delete your class?</label>
                                                        <form action="DeleteAClassController" method="post">
                                                            <div class="pp-footer">
                                                                <input type="submit" style="font-size: 40px; height: 60px;" class="mc-btn-3 btn-style-1" value="OK">
                                                                <a href="#" style="margin-top: 10px;padding: 15px 10px 18px 10px;font-size: 40px; height: 60px;" class="cancel mc-btn-3 btn-style-5">Cancel</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>



                            </div>
                            <!--END CLASS-->

                        </div>
                    </div>
                </div>
            </section>


            <!-- FOOTER -->
            <jsp:include page="Footer.jsp"></jsp:include>
            <!-- END / FOOTER -->





        </div>
        <!-- END / PAGE WRAP -->

        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
        <script>
                                                function gotoClass(s) {
                                                    var goto = document.getElementById(s);
                                                    goto.submit();
                                                }

                                                if ($('.popup-with-zoom-anim').length) {
                                                    $('.popup-with-zoom-anim').magnificPopup({
                                                        type: 'inline',

                                                        fixedContentPos: false,
                                                        fixedBgPos: true,

                                                        overflowY: 'auto',

                                                        closeBtnInside: true,
                                                        preloader: false,

                                                        midClick: true,
                                                        removalDelay: 300,
                                                        mainClass: 'my-mfp-zoom-in'
                                                    });
                                                    $('.design-course-popup').delegate('.cancel', 'click', function (evt) {
                                                        evt.preventDefault();
                                                        $('.mfp-close').trigger('click');
                                                    });
                                                }


        </script>
    </body>

</html>
