<%-- 
    Document   : dashBoardForQuiz
    Created on : Jul 21, 2022, 5:37:56 PM
    Author     : fptshop
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        
        <title>Quiz Admin</title>
    </head>
    <body id="page-top">

        <!-- PAGE WRAP -->
        <div id="page-wrap">

            <!-- PRELOADER -->
            <div id="preloader">
                <div class="pre-icon">
                    <div class="pre-item pre-item-1"></div>
                    <div class="pre-item pre-item-2"></div>
                    <div class="pre-item pre-item-3"></div>
                    <div class="pre-item pre-item-4"></div>
                </div>
            </div>
            <!-- END / PRELOADER -->

            <!-- HEADER -->
            <jsp:include page="AdminHeader.jsp"></jsp:include>
                <!-- END / HEADER -->


                <!-- PROFILE FEATURE -->
                <!-- SECTION 2 -->
                <section id="mc-section-2" class="mc-section-2 section">
                    <div class="awe-parallax bg-section1-demo"></div>
                    <div class="overlay-color-1"></div>
                    <div class="container">
                        <div class="section-2-content">
                            <div class="row">

                                <div class="col-md-5">
                                    <div class="ct">
                                        <h2 class="big">Learning online is easier than ever before</h2>
                                        <p class="mc-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        <a href="#" class="mc-btn btn-style-3" style="background-color:#012340; color: white; font-size: 28px;">${totalQuiz} QUIZZES</a>
                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="image">
                                    <img src="images/image.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- END / SECTION 2 -->
            <!-- END / PROFILE FEATURE -->


            <!-- CONTEN BAR -->


            <!-- END / CONTENT BAR -->


            <!-- COURSE CONCERN -->
            <section id="course-concern" class="course-concern">
                <div class="container">
                    <div class="table-asignment">
                        <div>
                            <H2 style="color:  gold; font-weight: 600; font-size: 36px; text-align: center">Admin Dashboard</H2>
                        </div>
                        <ul class="nav-tabs" role="tablist">
                            <li class="active"><a href="#mysubmissions" role="tab" data-toggle="tab">All Quiz</a></li>
                            <li><a href="#studentssubmissions" role="tab" data-toggle="tab">Quiz by Category</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- MY SUBMISSIONS -->
                            <div class="tab-pane fade in active" id="mysubmissions">
                                <!-- SEARCH BOX -->
                                <form action="SearchQuizByAdminController" method="Post">
                                    <div style="padding-top: 20px" class="search-box">
                                        <i width="80px" style="color: black;  " class="icon md-search"></i>
                                        <div class="search-inner">
                                            <form>
                                                <input oninput="searchByName(this)" name="txt" type="text" style="color:black" placeholder="key words">
                                            </form>
                                        </div>
                                    </div>
                                </form>
                                <!-- END / SEARCH BOX -->
                                <div class="table-wrap">
                                    <!-- TABLE HEAD -->
                                    <div class="table-head">
                                        <div class="submissions"></div>
                                        <div style="color: black" class="total-subm">By Account</div>
                                        <div class="replied"></div>
                                        <div style="color: black" class="latest-reply">Num of viewers</div>
                                        <div class="tb-icon"></div>
                                    </div>
                                    <!-- END / TABLE HEAD -->

                                    <!-- TABLE BODY -->
                                    <div class="table-body content">
                                        <!-- TABLE ITEM -->
                                        <c:forEach items = "${requestScope.listQuiz}" var = "x">
                                            <div class="table-item">
                                                <div class="thead">
                                                    <div style="color: black" class="submissions"><a href="ViewQuizDetailController?quizid=${x.getQuizid()}">${x.getTitle()}</a></div>
                                                    <div style="color: black" class="total-subm">${x.getEmail()}</div>
                                                    <div style="color: black" class="replied"></div>
                                                    <div style="color: black" class="latest-reply">${x.getNumOfViewers()}</div>
                                                </div>
                                                

                                                
                                            </div>
                                        </c:forEach>                                
                                        <!-- END / TABLE ITEM -->
                                    </div>
                                    <!-- END / TABLE BODY -->
                                </div>

                            </div>
                            <!-- END / MY SUBMISSIONS -->

                            <!-- MY SUBMISSIONS -->
                            <div class="tab-pane fade" id="studentssubmissions">
                                <div class="table-wrap">
                                    <!-- TABLE HEAD -->
                                    <div class="table-head">
                                        <div style="color: black; margin-left: 270px" class="total-subm">Category name</div>
                                        <div style="color: black; margin-left: 180px" class="total-subm">Num of Quiz by category</div>
                                        <div class="submissions"></div>
                                        <!--<div class="replied">Replied</div>-->
                                        <!--<div class="latest-reply">Latest Submissions</div>-->
                                        <div class="tb-icon"></div>
                                    </div>
                                    <!-- END / TABLE HEAD -->

                                    <!-- TABLE BODY -->
                                    <div class="table-body" style="margin-left: 300px">
                                        <c:forEach items = "${requestScope.listCate}" var = "z">
                                            <!-- TABLE ITEM -->
                                            <div class="table-item" style="width: 900px">
                                                <div class="thead">
                                                    <div style="color: black" class="submissions"><a href="#">${z.getCategoryName()}</a></div>
                                                    <div style="color: black" class="total-subm">${z.getNumOfQuizByCategory()}</div>
                                                    <div class="replied"></div>
                                                    <div class="latest-reply"></div>
                                                </div>

                                            </div>
                                        </c:forEach>
                                        <!-- END / TABLE ITEM -->
                                    </div>
                                    <!-- END / TABLE BODY -->
                                </div>
                            </div>
                            <!-- END / MY SUBMISSIONS -->

                        </div>

                    </div>
                </div>
            </section>
            <!-- END / COURSE CONCERN -->


            <!-- FOOTER -->
            <jsp:include page="Footer.jsp"></jsp:include>
            <!-- END / FOOTER -->



        </div>
        <!-- END / PAGE WRAP -->

        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script src="jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>

        <script type="text/javascript">

            function searchByName(param) {
                var txtSearch = param.value;
                console.log(txtSearch);
                $.ajax({
                    url: "/SWPPracticeQuiz_KS04/SearchQuizByAdminController",
                    type: "get",
                    data: {
                        txt: txtSearch
                    },
                    success: function (data) {
                        var row = document.getElementsByClassName("content")[0];
                        row.innerHTML = data;

                        //                                                            $("#content").html(data);
                    },
                    error: function (xhr) {
                        //asdfghjk
                    }

                });
            }

        </script>
    </body>

</html>
