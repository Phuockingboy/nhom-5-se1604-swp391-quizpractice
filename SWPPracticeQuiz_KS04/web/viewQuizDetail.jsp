<%-- 
    Document   : viewQuizDetail
    Created on : Jun 14, 2022, 1:20:23 PM
    Author     : fptshop
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <title>View Quiz Detail</title>
        <style>
            .instruction{
                display: none;
                color: green;
                font-weight: bold;
            }
            .modal-header, .close {

                color:white !important;
                text-align: center;
                font-size: 30px;
                font-weight: 600;
            }
            h4 {
                color:black;
                text-align: center;
                font-size: 30px;
            }
            .mmodal-header, .mmodal-header h4{
                background-color: #5bc0de;
                color:white !important;
                text-align: center;
                font-size: 30px;
            }
            .mmodal-footer {
                background-color: #f9f9f9;}
            .feedback{
                width: 100%;
                max-width: 780px;
                background: #fff;
                margin: 0 auto;
                padding: 15px;
                box-shadow: 1px 1px 16px rgba(0, 0, 0, 0.3);
            }
            .survey-hr{
                margin:10px 0;
                border: .5px solid #ddd;
            }
            .star-rating {
                margin: 25px 0 0px;
                font-size: 0;
                white-space: nowrap;
                display: inline-block;
                width: 175px;
                height: 35px;
                overflow: hidden;
                position: relative;
                background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
            }
            .star-rating i {
                opacity: 0;
                position: absolute;
                left: 0;
                top: 0;
                height: 100%;
                width: 20%;
                z-index: 1;
                background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
            }
            .star-rating input {
                -moz-appearance: none;
                -webkit-appearance: none;
                opacity: 0;
                display: inline-block;
                width: 20%;
                height: 100%;
                margin: 0;
                padding: 0;
                z-index: 2;
                position: relative;
            }
            .star-rating input:hover + i,
            .star-rating input:checked + i {
                opacity: 1;
            }
            .star-rating i ~ i {
                width: 40%;
            }
            .star-rating i ~ i ~ i {
                width: 60%;
            }
            .star-rating i ~ i ~ i ~ i {
                width: 80%;
            }
            .star-rating i ~ i ~ i ~ i ~ i {
                width: 100%;
            }
            .choice {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                text-align: center;
                padding: 20px;
                display: block;
            }
            span.scale-rating{
                margin: 5px 0 15px;
                display: inline-block;

                width: 100%;

            }
            span.scale-rating>label {
                position:relative;
                -webkit-appearance: none;
                outline:0 !important;
                border: 1px solid grey;
                height:33px;
                margin: 0 5px 0 0;
                width: calc(10% - 7px);
                float: left;
                cursor:pointer;
            }
            span.scale-rating label {
                position:relative;
                -webkit-appearance: none;
                outline:0 !important;
                height:33px;

                margin: 0 5px 0 0;
                width: calc(10% - 7px);
                float: left;
                cursor:pointer;
            }
            span.scale-rating input[type=radio] {
                position:absolute;
                -webkit-appearance: none;
                opacity:0;
                outline:0 !important;
                /*border-right: 1px solid grey;*/
                height:33px;

                margin: 0 5px 0 0;

                width: 100%;
                float: left;
                cursor:pointer;
                z-index:3;
            }
            span.scale-rating label:hover{
                background:#fddf8d;
            }
            span.scale-rating input[type=radio]:last-child{
                border-right:0;
            }
            span.scale-rating label input[type=radio]:checked ~ label{
                -webkit-appearance: none;

                margin: 0;
                background:#fddf8d;
            }
            span.scale-rating label:before
            {
                content:attr(value);
                top: 7px;
                width: 100%;
                position: absolute;
                left: 0;
                right: 0;
                text-align: center;
                vertical-align: middle;
                z-index:2;
            }
            .button-18 {
                align-items: center;
                background-color: #0A66C2;
                border: 0;
                border-radius: 100px;
                box-sizing: border-box;
                color: #ffffff;
                cursor: pointer;
                display: inline-flex;
                font-family: -apple-system, system-ui, system-ui, "Segoe UI", Roboto, "Helvetica Neue", "Fira Sans", Ubuntu, Oxygen, "Oxygen Sans", Cantarell, "Droid Sans", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Lucida Grande", Helvetica, Arial, sans-serif;
                font-size: 16px;
                font-weight: 600;
                justify-content: center;
                line-height: 20px;
                max-width: 480px;
                min-height: 40px;
                min-width: 0px;
                overflow: hidden;
                padding: 0px;
                padding-left: 20px;
                padding-right: 20px;
                text-align: center;
                touch-action: manipulation;
                transition: background-color 0.167s cubic-bezier(0.4, 0, 0.2, 1) 0s, box-shadow 0.167s cubic-bezier(0.4, 0, 0.2, 1) 0s, color 0.167s cubic-bezier(0.4, 0, 0.2, 1) 0s;
                user-select: none;
                -webkit-user-select: none;
                vertical-align: middle;
            }

            .button-18:hover,
            .button-18:focus { 
                background-color: #16437E;
                color: #ffffff;
            }

            .button-18:active {
                background: #09223b;
                color: rgb(255, 255, 255, .7);
            }

            .button-18:disabled { 
                cursor: not-allowed;
                background: rgba(0, 0, 0, .08);
                color: rgba(0, 0, 0, .3);
            }
            .instruction{
                display: none;
                color: green;
                font-weight: bold;
            }
            .back{
                margin-left: 55%;
                color: white;
                font-weight: bold;
                background: inherit;
                border: none;
            }
            .back:hover{
                color: greenyellow;
            }
            .link{
                color: black;
            }
            .link:visited{
                color: black;
            }
            .link:hover{
                color: blue;
                text-decoration: underline;
            }
            .popup-with-zoom-anim{
                padding-left: 40px;
                background-color: inherit;
                color: white; 
                text-align: left;

            }
            .popup-with-zoom-anim:hover{
                background-color: #0C406F;
            }
        </style>
    </head>

    <body id="page-top">

        <!-- PAGE WRAP -->
        <div id="page-wrap">

            <!-- PRELOADER -->
            <div id="preloader">

            </div>
            <!-- END / PRELOADER -->

            <!-- HEADER -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- END / HEADER -->



                <!-- COURSE -->
                <section class="course-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="sidebar-course-intro " style="margin-top: 100px; margin-left: 170px">
                                    <div style="margin-top: 20px;" class="study">
                                        <!--                                        <img width="50px" src="https://cdn-icons.flaticon.com/png/128/2824/premium/2824128.png?token=exp=1657783106~hmac=0a07a4d4d946dcd7843866e654b2f537" alt=""
                                                                                     srcset="">-->
                                        <span style="width: 100px; color: orange; font-size: 30px" class="glyphicon glyphicon-star-empty"></span>
                                        <span style="margin-left:30px; font-weight: 900; font-size: 35px; color: #0A66C2;">${average_rate}/5</span>
                                </div>
                                <div class="study" style="margin-top: 24px">
                                    <img width="35px" src="https://cdn-icons-png.flaticon.com/128/4064/4064243.png" alt=""
                                         srcset="">
                                    <span style="margin-left:10px; font-weight: 900; font-size: 18px;"><a id="myBtn" class="link">Rate</a></span>
                                    <c:if test = "${requestScope.userIdOfQuizId == sessionScope.user.getId()}">
                                        <div style="margin-top: 26px" class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog" style="text-align: center">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding:35px 50px;">
                                                        <h4 class="notice" style="color: red; font-weight: 600"><span class=""></span> ! Notice from system</h4>
                                                    </div>
                                                    <div class="modal-body" style="padding:40px 50px;">
                                                        <p style="font-size:22px; font-weight: 600">Hi ${sessionScope.user.getUserName()}, you can not rate your Quiz!</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div style="float:right">
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test = "${check!=null}">
                                        <div style="margin-top: 26px" class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog" style="text-align: center">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding:35px 50px;">
                                                        <h4 class="notice" style="color: orange; font-weight: 600"><span class=""></span> ! Notice from system</h4>
                                                    </div>
                                                    <div class="modal-body" style="padding:40px 50px;">
                                                        <p style="font-size:22px; font-weight: 600">Hi ${sessionScope.user.getUserName()}, you have rated this Quiz!</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div style="float:right">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test = "${requestScope.userIdOfQuizId != sessionScope.user.getId()}">
                                        <!-- Modal -->
                                        <div style="margin-top: 26px" class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header" style="padding:35px 50px;">
                                                        <h4>Hi ${sessionScope.user.getUserName()}, please rate the Quiz!</h4>
                                                    </div>
                                                    <div class="modal-body" style="padding:40px 50px;">
                                                        <form method="Post" action="RateAndFeedbackController">
                                                            <span style="color: black; margin-left: 22px">1. Your overall experience with us ?</span><br>
                                                            <span style="margin-left: 22px" class="star-rating">
                                                                <c:forEach begin = "0" end = "5" var = "i">
                                                                    <input type="radio" name="rating1" value="${i+1}" required><i></i>
                                                                    </c:forEach>
                                                                <!--                                                            <input type="radio" name="rating1" value="2"><i></i>
                                                                                                                                <input type="radio" name="rating1" value="3"><i></i>
                                                                                                                                <input type="radio" name="rating1" value="4"><i></i>
                                                                                                                                <input type="radio" name="rating1" value="5"><i></i>-->
                                                                <input hidden type="text" class="quizid" name="quizid" id="quizid" value=${requestScope.quiz.getQuizid()} >
                                                            </span>
                                                            <div class="clear"></div>
                                                            <br>
                                                            <div style="width: 600px;" class="">
                                                                <span style="color: black; margin-left: 22px" for="exampleFormControlTextarea1" class="form-label">2. Write your feedback</span>
                                                                <textarea name="feedback" required minlength="10" maxlength="100" style="height: 150px; width: 450px; margin-top:18px; margin-left: 22px"  class="form-control" ></textarea>
                                                            </div>
                                                            <br>
                                                            <div class="clear"></div>
                                                            <input style="background:#367ced;color:#fff;padding:12px;border:0; border-radius: 7px; margin-left: 20px; font-weight: 700" type="submit"
                                                                   value="Submit your review">
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div style="float:right">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div> 
                                    </c:if>
                                </div>
                                <div style="margin-top: 20px;">
                                    <img width="35px" src="https://cdn-icons-png.flaticon.com/128/3068/3068380.png" alt=""
                                         srcset="">

                                    <span style="color: black;margin-left:10px; font-weight: 900; font-size: 18px; "><a href="LearnController?quizid=${requestScope.quiz.quizid}" class="link">Learn</a></span>

                                </div>
                                <div style="margin-top: 20px;">
                                    <img width="35px" src="https://cdn-icons-png.flaticon.com/512/1436/1436722.png" alt=""
                                         srcset="">
                                    <span style="color: black; margin-left:10px; font-weight: 900; font-size: 18px; width: 80px; padding:0;" class="btn-basic btn-lg"><a id="myTest" style="" class="link">Take Test</a></span>
                                </div>
                                <div style="margin-top: 20px;">
                                    <img width="35px" src="https://cdn-icons-png.flaticon.com/512/1436/1436722.png" alt=""
                                         srcset="">
                                    <span style="color: black;margin-left:10px; font-weight: 900; font-size: 18px;"><a href="ViewTestResultController?quizid=${requestScope.quiz.quizid}" class="link">Test Results</a></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="margin-left: 60px">
                            <div class="sidebar-course-intro">
                                <b class="breadcrumb">
                                    Quiz name - ${requestScope.quiz.getTitle()}
                                </b>  
                                <div style="text-align: center;" class="video-course-intro">
                                    <div class="flip-card">
                                        <div class="flip-card-inner">
                                            <div class="flip-card-front">
                                                <div class="Xcontainer">
                                                    ${requestScope.question.getTitle()}
                                                </div>
                                            </div>
                                            <div class="flip-card-back">
                                                <div class="Xcontainer">
                                                    ${requestScope.question.getRightAnswer()}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <a style="margin-left: 250px; " href="ViewQuizDetailController?quizid=${quizid}&currentIndex=${currentIndex-1}"><img
                                        width="30px" src="https://cdn-icons-png.flaticon.com/128/2879/2879564.png"
                                        alt=""></a>
                                <span style="color: black; font-size: 18px;"> ${currentIndex}/${requestScope.quiz.getListQuestions().size()} </span>
                                <a href="ViewQuizDetailController?quizid=${quizid}&currentIndex=${currentIndex+1}"><img
                                        width="30px" src="https://cdn-icons-png.flaticon.com/128/2879/2879593.png"
                                        alt=""></a>

                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- Modal Test -->
            <form action="CreateTestController" method="post" style="margin-top: 0;margin-bottom: 0;">
                <div class="modal fade" id="testModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="mmodal-header" style="padding:35px 50px;">
                                <button style="font-size: 60px; color: black" type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4><span class="glyphicon glyphicon-book"></span> Do you want to start take test?</h4>
                            </div>
                            <div class="modal-body" style="padding:40px 50px;">
                                <input name="quizid" value="${quizid}" hidden>
                                <div class="form-group">
                                    <label for="usrname" style="font-size: 25px; font-weight: bold;"><span class="glyphicon glyphicon-pencil"></span> Set question limit</label>
                                    <input style="width: 300px; border-radius: 7px" min="1" max="${totalNumberOfQuiz}" type="number" class="form-control" name="totalNumberOfQuiz" placeholder="Number of Questions" required="">
                                </div>
                                <div class="form-group" style="font-size: 25px;">
                                    <label for="psw" style="font-size: 25px; font-weight: bold;"><span class="glyphicon glyphicon-time"></span> Set time limit</label><br>
                                    <input min="01" max="240" name="minute" type="number" value="00" style="font-size: 25px;width: 70px; height: 35px; border-radius: 7px;"> Minutes
                                </div>
                            </div>
                            <div class="modal-footer mmodal-footer">
                                <div style="float:right">
                                    <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">No</button>
                                    <button type="submit" class="btn btn-primary btn-lg">Yes</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
<!--                                <div style="font-weight: bold;text-align: center;">${noti}</div>
            <h5 style="color: black; font-weight: bolder;margin-top: 30px;text-align: center">All Test Result</h5>
            <table class="table" border="0" style="text-align: center; " >
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr >
                        <td scope="col">Title</td>
                        <td scope="col">Number Of Question</td>
                        <td scope="col">Time</td>
                        <td scope="col">Correct Answer</td>
                        <td scope="col">Mark</td>
                        <td>Date Created</td>
                    </tr>
                    <c:forEach var="o" items="${listTest}">

                        <tr>
                            <td scope="row">${o.title}</td>
                            <td>${o.numberOfQuestion}</td>
                            <td>${o.time}</td>
                            <td>${o.correctAnswer}</td>
                            <td>${o.mark}</td>
                            <td>${o.dateCreated}</td>
                            <td> <a href="ViewTestResultDetailController?testid=${o.testid}">View Result</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>-->
            <!-- END / COURSE TOP -->
            <div style="margin-left: 125px;" class="tabs-page col-md-10">
                <ul class="nav-tabs" role="tablist">

                    <li class="active"><a href="#quizdetail" role="tab" data-toggle="tab">Quiz Details</a></li>
                    <li><a href="#introduction" role="tab" data-toggle="tab">Introduction</a></li>
                    <li><a href="#review" role="tab" data-toggle="tab">Review</a></li>
                    <!--<li><a href="#conment" role="tab" data-toggle="tab">Conment</a></li>-->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!--INTRODUCTION--> 
                    <div class="tab-pane fade" id="introduction">
                        <h3 class="sm black bold">Introduction</h3>
                        <p>${requestScope.quiz.description}</p>

                    </div>
                    <!--END / INTRODUCTION--> 

                    <!-- Term -->
                    <div class="tab-pane fade in active" id="quizdetail">
                        <!-- SECTION Quizdetails -->
                        <div class="section-outline">
                            <h4 class="tit-section xsm">Questions in this Quiz (${requestScope.quiz.getListQuestions().size()})</h4>
                            <c:forEach begin = "0" end = "${requestScope.quiz.getListQuestions().size()-1}" var = "i">
                                <ul class="section-list">
                                    <li>
                                        <div style="width: 200px;" class="count"><span>${requestScope.quiz.getListQuestions().get(i).getRightAnswer()}</span></div>
                                        <div class="list-body">
                                            <p>
                                                <a href="#" style="font-size: 18px; font-weight: bold;">
                                                    ${requestScope.quiz.getListQuestions().get(i).getTitle()}</br>
                                                </a>
                                            </p>
                                            <c:forEach begin = "0" end = "${requestScope.quiz.getListQuestions().get(i).getListOptions().size()-1}" var = "x">
                                                <br>${requestScope.s.charAt(x)}.      ${requestScope.quiz.getListQuestions().get(i).getListOptions().get(x).getContent()}
                                            </c:forEach>
                                            <br><span class = "instruction" id="${i}">Instruction: ${requestScope.quiz.getListQuestions().get(i).intruction}</span>
                                        </div>
                                        <a id="showanswer${i}" onclick="showInstruction('${i}')" class="mc-btn-2 btn-style-2">Instruction</a>
                                        <a id="hideanswer${i}" style="display: none;" onclick="hideInstruction('${i}')" class="mc-btn-2 btn-style-2">Close Instruction</a>
                                    </li>
                                </ul>
                            </c:forEach>
                        </div>
                        <!-- END / SECTION OUTLINE -->
                    </div>
                    <!-- END / OUTLINE -->
                    <!-- REVIEW -->
                    <div class="tab-pane fade" id="review">
                        <c:if test="${check==null && quiz.userid!=sessionScope.user.getId()}">
                            <div class="total-review">
                                <h3 style="color: orangered; font-weight:600" class="md black">Your haven't rate this Quiz</h3>
                            </div>
                        </c:if>
                        <c:if test="${check!=null}">
                            <div class="total-review">
                                <h3 class="md black">Your recent review</h3>
                            </div>
                            <ul class="list-review">            
                                <li class="review">
                                    <div class="body-review">
                                        <div class="review-author">
                                            <a href="#">
                                                <img src="images/team-13.jpg" alt="">
                                                <i class="icon md-email"></i>
                                                <i class="icon md-user-plus"></i>
                                            </a>
                                        </div>
                                        <div class="content-review">
                                            <h3 class="sm black">
                                                <a href="#">Username - ${sessionScope.user.userName}</a>
                                            </h3>

                                            <div class="rating">
                                                <c:forEach begin="1" end ="${check.rate}" var="m">
                                                    <a href="#" class="active"></a>
                                                </c:forEach>
                                                <c:forEach begin="1" end ="${5-check.rate}" var="n">
                                                    <a href="#"></a>
                                                </c:forEach>
                                            </div>
                                            <p>${requestScope.check.getFeedback()}</p>
                                        </div>
                                    </div>
                                    <button class="button-18" id="myBtn1" role="button" style="float: right; margin-right: 0;">Edit your feedback</button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal1" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header" style="padding:35px 50px;">
                                                    <h4><span class=""></span>Edit rate and feedback for this Quiz</h4>
                                                </div>
                                                <div class="modal-body" style="padding:40px 50px;">
                                                    <form method="Post" action="ViewQuizDetailController">
                                                        <span style="color: black; margin-left: 22px">1. Your overall experience with us ?</span><br>
                                                        <span style="margin-left: 22px" class="star-rating">
                                                            <input type="radio" name="ratingEdit" value="1"><i></i>
                                                            <input type="radio" name="ratingEdit" value="2"><i></i>
                                                            <input type="radio" name="ratingEdit" value="3"><i></i>
                                                            <input type="radio" name="ratingEdit" value="4"><i></i>
                                                            <input type="radio" name="ratingEdit" value="5"><i></i>
                                                            <input hidden type="text" name="quizid" value=${requestScope.quiz.getQuizid()}>
                                                        </span>
                                                        <div class="clear"></div>
                                                        <br>
                                                        <div style="width: 600px;" class="">
                                                            <span style="color: black; margin-left: 22px" for="exampleFormControlTextarea1" class="form-label">2. Write your feedback</span>
                                                            <textarea name="feedbackEdit" style="height: 150px; width: 450px; margin-top:18px; margin-left: 22px"  class="form-control" ></textarea>
                                                        </div>
                                                        <br>
                                                        <div class="clear"></div>
                                                        <input style="background:#367ced;color:#fff;padding:12px;border:0; border-radius: 7px; margin-left: 20px; font-weight: 700" type="submit"
                                                               value="Save changes">
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <div style="float:right">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div> 

                                </li>
                            </ul>
                        </c:if>
                        <div class="total-review">
                            <h3 class="md black">${requestScope.listRateAndFeedbacks.size()} Reviews Total</h3>
                        </div>
                        <ul id="content" class="list-review contentOfQuiz" name="loadmore">            
                            <c:forEach items = "${requestScope.Top3RateAndFeedbacks}" var = "i">
                                <li class="review comment">
                                    <div class="body-review">
                                        <div class="review-author">
                                            <a href="#">
                                                <img src="images/team-13.jpg" alt="">
                                                <i class="icon md-email"></i>
                                                <i class="icon md-user-plus"></i>
                                            </a>
                                        </div>
                                        <div class="content-review">
                                            <h3 class="sm black">
                                                <a href="#">Username - ${i.getUsername()}</a>
                                            </h3>
                                            <div class="rating">
                                                <c:forEach begin="1" end ="${i.getRate()}" var="j">
                                                    <a href="#" class="active"></a>
                                                </c:forEach>
                                                <c:forEach begin="1" end ="${5-i.getRate()}" var="z">
                                                    <a href="#"></a>
                                                </c:forEach>
                                            </div>
                                            <p>${i.getFeedback()}</p>
                                        </div>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                        <input hidden class="totalcomment" name="rateId" value="${listRateAndFeedbacks.size()}">
                        <c:if test = "${requestScope.listRateAndFeedbacks.size()>3}">
                            <div style="text-align:center">
                                <!--<form name="loadmore">-->
                                    <!--<input hidden type="text" name="quizid" value=${requestScope.quiz.getQuizid()}>-->
                                <button  class="loadmorebutton" onclick="loadMore(${requestScope.quiz.getQuizid()})" 
                                         style="background:#367ced;color:#fff;padding:12px;border:0; border-radius: 7px; margin-left: 20px; font-weight: 700; ">
                                    Load more comments  
                                </button>
                                <!--</form>-->
                            </div>
                        </c:if>
                    </div>
                    <!-- END / REVIEW -->
                </div>
            </div>

            <!-- COURSE CONCERN -->
            <section id="course-concern" class="course-concern">
                <div class="container">
                   
                </div>
            </section>
            <!-- END / COURSE CONCERN -->


            <!-- FOOTER -->
            <jsp:include page="Footer.jsp"></jsp:include>
            <!-- END / FOOTER -->





        </div>
        <!-- END / PAGE WRAP -->


        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script src="jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>-->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
        <script>
                                    if ($('.popup-with-zoom-anim').length) {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',

                    fixedContentPos: false,
                    fixedBgPos: true,

                    overflowY: 'auto',

                    closeBtnInside: true,
                    preloader: false,

                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });
                $('.design-course-popup').delegate('.cancel', 'click', function (evt) {
                    evt.preventDefault();
                    $('.mfp-close').trigger('click');
                });
            }
    
    function loadMore(s) {
//                                        var quizid = document.getElementsByClassName("quizid");
//                                        console.log(quizid);
                                        var count = document.getElementsByClassName("comment").length;
                                        var count1 = parseInt(count) + 3;
                                        var counttotal = document.getElementsByClassName("totalcomment")[0].value;
                                        var count2 = parseInt(counttotal);

                                        if (count1 >= count2) {
                                            console.log("count");
                                            document.getElementsByClassName("loadmorebutton")[0].style.display = "none";
                                        } else {
                                            console.log("count1");
                                        }
                                        console.log(count);
                                        console.log(counttotal);
//                                        console.log(s);
                                        $.ajax({
                                            url: "/SWPPracticeQuiz_KS04/LoadMoreController",
                                            type: "get",
                                            data: {
                                                count: count,
                                                quizid: s
                                            },
                                            success: function (data) {
                                                var row = document.getElementsByClassName("contentOfQuiz")[0];
                                                row.innerHTML += data;

//                                                            $("#content").html(data);
                                            },
                                            error: function (xhr) {
                                                //asdfghjk
                                            }

                                        });



                                    }
                                    function showInstruction(s) {
                                        var showanswer = "showanswer" + s;
                                        var hideanswer = "hideanswer" + s;
                                        document.getElementById(s).style.display = 'inherit';
                                        document.getElementById(showanswer).style.display = 'none';
                                        document.getElementById(hideanswer).style.display = 'inherit';
                                    }
                                    function hideInstruction(s) {
                                        var showanswer = "showanswer" + s;
                                        var hideanswer = "hideanswer" + s;
                                        console.log(s);
                                        document.getElementById(s).style.display = 'none';
                                        document.getElementById(showanswer).style.display = 'inherit';
                                        document.getElementById(hideanswer).style.display = 'none';
                                    }
                                    $(document).ready(function () {
                                        $("#myBtn").click(function () {
                                            $("#myModal").modal();
                                        });
                                    });
                                    $(document).ready(function () {
                                        $("#myBtn1").click(function () {
                                            $("#myModal1").modal();
                                        });
                                    });
                                    $(document).ready(function () {
                                        $("#myTest").click(function () {
                                            $("#testModal").modal();
                                        });
                                    });



        </script>
    </body>


</html>