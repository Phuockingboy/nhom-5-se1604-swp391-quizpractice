<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">
        <title>Home QuizPractice</title>
        <style>
            .search-button{
                margin-left: 20px;
                border-radius: 5px;
                background-color: #0a6bff;
                color: white;
                font-size: 20px;
                height: 35px;
            }
            .search-button:hover{
                background-color: #065dd8;
            }
            .popup-with-zoom-anim{
                padding-left: 40px;
                background-color: inherit;
                color: white; 
                text-align: left;

            }
            .popup-with-zoom-anim:hover{
                background-color: #0C406F;
            }
        </style>
    </head>
    <body id="page-top" class="home">

        <!-- PAGE WRAP -->
        <div id="page-wrap">

            <!-- PRELOADER -->
            <div id="preloader">
                <div class="pre-icon">
                    <div class="pre-item pre-item-1"></div>
                    <div class="pre-item pre-item-2"></div>
                    <div class="pre-item pre-item-3"></div>
                    <div class="pre-item pre-item-4"></div>
                </div>
            </div>
            <!-- END / PRELOADER -->

            <!-- HEADER -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- SUB BANNER -->
                <section class="sub-banner section">
                    <div class="awe-parallax bg-profile-feature"></div>
                    <div class="awe-overlay overlay-color-3"></div>
                    <div class="container">
                        <div class="sub-banner-content">
                            <h2 class="big">Welcome to Quiz Practice</h2>
                            <a href="HomeController" class="mc-btn btn-style-3">See course</a>
                        </div>
                    </div>
                </section>
                <!-- END / SUB BANNER -->


                <!-- PAGE CONTROL -->
                <section class="page-control">
                    <div class="container">
                        <div class="page-info">
                            <a href="HomeController"><i class="icon md-arrow-left"></i>Back to home</a>
                            <!--<div> <h3 style="color: red">${message}</h3></div>-->
                    </div>

                    <div class="page-view">
                        View
                        <span class="page-view-info view-grid active" title="View grid"><i class="icon md-ico-2"></i></span>
                        <span class="page-view-info view-list" title="View list"><i class="icon md-ico-1"></i></span>
                    </div>
                </div>
            </section>

            <section id="course-concern" class="course-concern">
                <div class="container">

                    <div class="row">
                        <div class="col-md-10 col-md-push-1">
                            <div class="row" style="margin-top: 30px;">
                                <div class="col-md-8">
                                    <nav aria-label="...">

                                        <c:if test="${endPage>=2}">
                                            <ul class="pagination">
                                                <c:if test="${index >= 4 }">
                                                    <li class="page-item">
                                                        <a class="page-link" href="HomeController?index=${1}&txtSearch=${txtSearch}">1</a>
                                                    </li>
                                                    <c:if test="${index > 4 }">
                                                        <li class="page-item">
                                                            <a class="page-link" href="#">...</a>
                                                        </li>
                                                    </c:if>
                                                </c:if>

                                                <c:if test="${index -2 >=1 }">
                                                    <li class="page-item">
                                                        <a class="page-link" href="HomeController?index=${index-2}&txtSearch=${txtSearch}">${index-2}</a>
                                                    </li>
                                                </c:if>
                                                <c:if test="${index -1 >=1 }">
                                                    <li class="page-item">
                                                        <a class="page-link" href="HomeController?index=${index-1}&txtSearch=${txtSearch}">${index-1}</a>
                                                    </li>
                                                </c:if>
                                                <li class="page-item active">
                                                    <a class="page-link" href="HomeController?index=${index}&txtSearch=${txtSearch}">${index}</a>
                                                </li>
                                                <c:if test="${index +1 <= endPage }">
                                                    <li class="page-item">
                                                        <a class="page-link" href="HomeController?index=${index+1}&txtSearch=${txtSearch}">${index+1}</a>
                                                    </li>
                                                </c:if>
                                                <c:if test="${index +2 <= endPage }">
                                                    <li class="page-item">
                                                        <a class="page-link" href="HomeController?index=${index+2}&txtSearch=${txtSearch}">${index+2}</a>
                                                    </li>
                                                </c:if>
                                                <c:if test="${index +3 < endPage }">
                                                    <li class="page-item">
                                                        <a class="page-link" href="#">...</a>
                                                    </li>
                                                </c:if>
                                                <c:if test="${index +3 <= endPage }">
                                                    <li class="page-item">
                                                        <a class="page-link" href="HomeController?index=${index+2}&txtSearch=${txtSearch}">${endPage}</a>
                                                    </li>
                                                </c:if>

                                            </ul><span style="margin-top: 10px; padding-top: 20px; font-size: 17px"><form action="HomeController" method="post" >
                                                    <input type="text" name="txtSearch" value="${txtSearch}" hidden="">
                                                    Goto:
                                                    <span><input name="index" type="number" min="1" max="${endPage}"></span> /${endPage}
                                                    <span><input type="submit" value="Go"></span>

                                                </form></span>
                                            </c:if>



                                    </nav>

                                </div>

                                <div class="col-md-4" style="margin-top: 18px;" >

                                    <form style="margin-left: 0;" action="HomeController" method="post">
                                        <input style="margin-left: 0;"type="text" name="index" value="1" hidden="" >
                                        <input style="border:2px solid black;
                                               border-radius: 5px;padding-left: 5px; font-size: 20px; height: 35px; font-weight: bold; " 
                                               type="Text" name="txtSearch" value="${txtSearch}" placeholder="">
                                        <span><button class= "search-button" type="submit" name="btn-search" class="">Search</button></span>
                                    </form> 
                                </div>
                            </div>
                            <div class="content grid">
                                <div class="row" >
                                    <!-- ITEM -->
                                    <c:forEach items = "${listQuiz}" var = "o">
                                        <div class="col-sm-6 col-md-4">
                                            <div class="mc-item mc-item-2" style="">
                                                <div class="meta-categories" ><a href="#" style=" width: 100%; background-color: white; color: black; font-weight:bolder ">${o.title}</a></div>
                                                <div class="content-item " style="padding-top: 40px;">
                                                    <h4><a href="#"> ${o.description}</a></h4>
                                                    <div class="name-author">
                                                        By <a href="ViewOtherQuizController?userid=${o.userid}">${o.username}</a>
                                                    </div>
                                                    <div class="ft-item">
                                                        <div class="">
                                                            <span>
                                                                <!--                                                                <img style="margin-left:14px" width="50px" src="https://cdn-icons.flaticon.com/png/512/2824/premium/2824128.png?token=exp=1658340617~hmac=7b3eac40e5a8f681aaad7c31635aead1" alt="">-->
                                                                <!--<span style="width: 100px; color: orange; font-size: 15px" class="glyphicon glyphicon-star-empty"></span>-->
                                                                <div class="rating">
                                                                    
                                                                    <a href="#" <c:if test="${o.getAvgOfRate()>=0.5}">class="active"</c:if> ></a>
                                                                    <a href="#" <c:if test="${o.getAvgOfRate()>=1.5}">class="active"</c:if> ></a>
                                                                    <a href="#" <c:if test="${o.getAvgOfRate()>=2.5}">class="active"</c:if> ></a>
                                                                    <a href="#" <c:if test="${o.getAvgOfRate()>=3.5}">class="active"</c:if> ></a>
                                                                    <a href="#" <c:if test="${o.getAvgOfRate()>=4.5}">class="active"</c:if> ></a>
                                                                </div>
                                                                <span style="font-weight:600; font-size:22px; margin-left: 15px">${o.getAvgOfRate()} of 5</span>
                                                            </span>
                                                        </div>
                                                        <div>
                                                            <a href="ViewQuizDetailController?quizid=${o.quizid}"><button class="button-66" role="button">View Quiz</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END / ITEM -->
                                    </c:forEach>

                                </div>


                            </div>
                        </div>
                    </div>

            </section>
            <!-- END / HOME SLIDER -->
            <!-- FOOTER -->
            <jsp:include page="Footer.jsp"></jsp:include>
            <!-- END / FOOTER -->





        </div>
        <!-- END / PAGE WRAP -->

        <!-- Load jQuery -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>
        <script>
            if ($('.popup-with-zoom-anim').length) {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',

                    fixedContentPos: false,
                    fixedBgPos: true,

                    overflowY: 'auto',

                    closeBtnInside: true,
                    preloader: false,

                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });
                $('.design-course-popup').delegate('.cancel', 'click', function (evt) {
                    evt.preventDefault();
                    $('.mfp-close').trigger('click');
                });
            }
        </script>
    </body>
</html>