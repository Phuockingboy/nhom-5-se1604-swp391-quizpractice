<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <!-- Google font -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
        <!-- Css -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/library/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/md-font.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/library/magnific-popup.css">

        <title>QuizPractice View Other Profile</title>
        <style>
            .link{
                color:black;
                font-size: 20px;
                font-weight: bold;
            }
            .link:visited{
                color:black;
            }
            .link:hover{
                color:blue;
                text-decoration: underline;
            }
            .navbarr{
                margin: 0;
                background-color: #eee;
                padding: 30px 150px 0px 150px;
                font-size: 20px;
                font-weight: bold;
            }
            .search-button{
                margin-left: 20px;
                border-radius: 5px;
                background-color: #0a6bff;
                color: white;
                font-size: 20px;
                height: 35px;
            }
            .search-button:hover{
                background-color: #065dd8;
            }
            div.b{
                border-radius: 5px; 
                margin-bottom: 20px; 
                padding: 10px 20px;
                border: 1px solid #BFBFBF;
                background-color: white;
                box-shadow: 10px 10px 5px #aaaaaa;
            }
            .dropbtn {
                background-color: white;
                color: black;
                font-size: 20px;
                border: none;
                cursor: pointer;
            }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                right: 0;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown-content a:hover {background-color: #f1f1f1;}

            .dropdown:hover .dropdown-content {
                display: block;
            }
            .modal-header, h4, .close {
                background-color: #094986;
                color:white !important;
                text-align: center;
                font-size: 30px;
            }
            #form-group{
                background-color: white;
            }
            .popup-with-zoom-anim{
                padding-left: 0px;
                background-color: inherit;
                color: black; 
                text-align: left;

            }
            .addM{
                background-color: #094986;
                margin-top: 33px; 
                padding: 20px 25px; 
                width: 150px;
            }
            .addM:hover{
                background-color: lightskyblue;
            }
            h5 {
                color:red !important;
                text-align: center;
                font-size: 30px;
            }
            /*            .popup-with-zoom-anim:hover{
                            background-color: #0C406F;
                        }*/
        </style>
    </head>
    <body id="page-top" class="home">

        <!-- PAGE WRAP -->
        <div id="page-wrap">
            <!-- HEADER -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- END / HEADER -->


                <!-- SUB BANNER -->
                <section class="sub-banner section">
                    <div class="awe-parallax bg-profile-feature"></div>
                    <div class="awe-overlay overlay-color-3"></div>
                    <div class="container">
                        <div class="info-author">

                            <div class="name-author">
                                <p style="font-weight: bold; font-size: 30px;">${classOfId.name}</p>
                        </div>     
                        <div class="address-author">
                            <p style="font-size: 15px;">Members: ${totalMember}</p>
                        </div>
                        <div class="address-author">
                            <p style="font-size: 15px;"><a href="ViewOtherQuizController?userid=${classOfId.createid}"> Created By: ${creater.userName}</a></p>
                        </div>

                    </div>
                        <c:if test="${isAdmin==true}">
                        <p style="color: white;">Allow Member Create Quiz : <c:if test="${classOfId.allowMemberCreateQuiz==true}">Yes</c:if>
                        <c:if test="${classOfId.allowMemberCreateQuiz==false}">No</c:if>
                            </p>
                            <span><a href="#AllowMemberCreateQuiz" class="link mc-btn-3 btn-style-1 popup-with-zoom-anim" style="color: aquamarine;" > Change allow members create Quiz</a></span>

                            <div class="modal-content design-course-popup pp-add-section zoom-anim-dialog mfp-hide" id="AllowMemberCreateQuiz" >
                                <div class="modal-header" style="padding:25px 40px;">
                                    <h4><span class="glyphicon glyphicon-user"></span> <b>Allow Create Quiz In Class?</b></h4>
                                </div>
                                <div class="modal-body" style="padding:40px 50px;">
                                    <form action="ChangeAllowCreateQuizInClass" method="post" id="formAllowCreateQuiz">
                                        <div style=" border: none;
                                             font-size: 20px;
                                             font-weight: bold;
                                             color: black;">
                                            Allow Member Create Quiz: <span>    <select name="allow" id="allow">
                                                    <option value="yes" <c:if test="${classOfId.allowMemberCreateQuiz==true}">selected</c:if>>Yes</option>
                                                <option value="no" <c:if test="${classOfId.allowMemberCreateQuiz==false}">selected</c:if> >No</option>
                                                </select></span>
                                        </div>
                                        <input name="classid" value="${classid}" hidden>
                                    <button type="submit" class=" addM btn btn-success btn-block" style="" >OK</button>
                                </form>
                            </div>
                        </div>
                    </c:if>
                    <div class="info-follow" style="margin-right: 100px;">
                        <div class="trophies">
                            <span style="font-weight: bold; font-size: 30px;">${classOfId.getCode()}</span>
                            <p>Code</p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END / SUB BANNER -->


            <!-- PAGE CONTROL -->
            <section class="page-control">
                <div class="container">
                    <div class="page-info" >
                        <a href="HomeController" class='link' ><i class="icon md-arrow-left"></i>Back to home</a>
                    </div>
                    <div class="page-info" >
                        <a href="ViewMyClassesController" class='link'> <span>|   My Classes</span></a>
                    </div>
                    <div class="page-info" >
                        <a href="ViewQuizInClassController?classid=${classOfId.classid}" style="color: #A5A5A5;"> <span>|  ${classOfId.name}</span></a>
                    </div>
                    <div class="page-view">
                        View
                        <span class="page-view-info view-grid" title="View grid"><i class="icon md-ico-2"></i></span>
                        <span class="page-view-info view-list  active" title="View list"><i class="icon md-ico-1"></i></span>
                    </div>
                </div>
            </section>
            <!-- END / PAGE CONTROL -->

            <section class="navbarr page-control">
                <div class="container">
                    <div class="page-info" >
                        <a href="ViewQuizInClassController?classid=${classid}&task=viewAllQuiz" class='link'> Quiz In Class</a>
                    </div>
                    <div class="page-info" >
                        <a href="ViewMemberInClassController?classid=${classid}" style="color: #A5A5A5;"> |   Members</a>
                    </div>
                    <div class="page-info" >
                        <a href="ViewQuizInClassController?classid=${classid}&task=viewOwnQuizInClass" class='link'> <span>| Created by you</span></a>
                    </div>
                </div>
            </section>


            <!-- CATEGORIES CONTENT -->
            <section id="categories-content" class="categories-content">
                <div class="container">
                    <div class="row" >
                        <!--<div class="col-md-1"></div>-->
                        <div class="col-md-8 col-md-push-1">
                            <div class="content list">
                                <div class="row">
                                    <div style="margin-top: 40px; margin-bottom: 20px; float: right; margin-right: 30px;" >
                                        <form action="ViewMemberInClassController" method="POST">
                                            <input type="text" name="task" value="${task}" hidden="">
                                            <input type="text" name="classid" value="${classid}" hidden="">
                                            <input style="border:2px solid black;
                                                   border-radius: 5px;padding-left: 5px; font-size: 20px; height: 35px; font-weight: bold; " 
                                                   type="Text" name="txtSearch" value="${txtSearch}" placeholder="">
                                            <span><button class= "search-button" type="submit" name="btn-search" class="">Search</button></span>
                                        </form> 
                                    </div>
                                </div>
                                <p style="color: green">${messageDelete1}</p>
                                <p style="color: red">${messageDelete2}</p>
                                <div class="row" id="content" style="padding-top: 18px; padding-right: 40px;border-right: 3px solid #BFBFBF;" >
                                    <!-- ITEM -->

                                    <c:forEach items="${listClassUser}" var="o">

                                        <div class='row b' >

                                            <div style="width: 80%; margin-left: 0; margin-right: 0; float: left">
                                                <div class="meta-categories" style="margin-bottom: 20px; font-size: 25px; font-weight: bold;">
                                                    <c:if test="${o.userid == sessionScope.user.id}">You:</c:if>${o.username}</div>
                                                <p style='color: black;'>Date Join: ${o.dateJoin}</p>
                                                <p style='color: black;'>
                                                    <c:if test="${o.joinBy == 0}">Joined By: Code</c:if>
                                                    <c:if test="${o.joinBy !=0 && o.joinBy !=o.userid}"><a href="ViewOtherQuizController?userid=${o.joinBy}">Added By: ${o.usernameAdd} </a></p></c:if>
                                                <c:if test="${o.joinBy == o.userid && o.joinBy != sessionScope.user.id}"><a href="ViewOtherQuizController?userid=${o.joinBy}">Creator: ${o.usernameAdd}  </a></p></c:if>
                                                <c:if test="${o.joinBy == o.userid && o.joinBy == sessionScope.user.id}"><p style="color: green; font-weight: bold;">You: Creator</p></c:if>
                                                <c:if test="${o.isAdmin == true && o.joinBy != o.userid}">
                                                    <p style='color: Green;'>Admin</p>
                                                </c:if>
                                            </div>
                                            <div style="width: 10%; float:right; margin-top: 10px; margin-right:0px; margin-left: 0">
                                                <div class="dropdown" style="float:right;">
                                                    <button class="dropbtn"><i class="glyphicon glyphicon-option-horizontal"></i></button>
                                                    <div class="dropdown-content">
                                                        <a href="ViewOtherQuizController?userid=${o.userid}">View Profile</a>
                                                        <c:if test="${isAdmin == true && o.isAdmin == false}">
                                                            <a href="DeleteMemberOfAClassController?userid=${o.userid}&classid=${classid}" onclick="return confirm('Are you sure you want to remove ${o.username}?');"
                                                               >Remove Member</a>
                                                        </c:if>
                                                        <c:if test="${o.isAdmin == true && classOfId.createid==sessionScope.user.id && o.userid != sessionScope.user.id }">
                                                            <a href="DeleteMemberOfAClassController?userid=${o.userid}&classid=${classid}" onclick="return confirm('Are you sure you want to remove ${o.username}?');"
                                                               >Remove Member</a>
                                                        </c:if>
                                                        <c:if test="${classOfId.createid==sessionScope.user.id && o.userid != sessionScope.user.id && o.isAdmin==true && o.userid != classOfId.createid}">
                                                            <a href="ChangeAdminRoleInClassController?userid=${o.userid}&classid=${classid}&isAdmin=false" onclick="return confirm('Are you sure you want to remove ${o.username}?');"
                                                               >Remove admin</a>
                                                        </c:if>
                                                        <c:if test="${classOfId.createid==sessionScope.user.id && o.userid != sessionScope.user.id && o.isAdmin==false}">
                                                            <a href="ChangeAdminRoleInClassController?userid=${o.userid}&classid=${classid}&isAdmin=true" onclick="return confirm('Are you sure you want to remove ${o.username}?');"
                                                               >Set as admin</a>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </c:forEach>
                                </div>


                            </div>
                        </div>

                        <div class="col-md-3 col-md-push-1 box" style="margin: 120px 5px 20px 20px;display: block;" >
                            <div>
                                <h2 style="color: #013351; font-size: 25px; font-weight: bold;"> Class Code: <span> ${classOfId.getCode()}</span></h2> 
                            </div>
                            <div class="meta-categories" >
                                <p><a href="MoveOutOfClassController?classid=${classid}"  class="link" ><i class="glyphicon glyphicon-log-out"></i><span> Out Class</span></a></p>
                            </div>
                            <c:if test="${isAdmin==false}">
                                <div class="meta-categories">
                                    <!--<p><a href="#" class="link" ><i class="icon md-user-plus"></i><span> Add member</span></a></p>-->
                                    <div class="add-section">

                                        <p><a href="#cannotAddMember" class=" link addMember mc-btn-3 btn-style-1 popup-with-zoom-anim" ><i class="icon md-user-plus "></i><span>Add member</span></a></p>
                                        <div id="cannotAddMember" class="design-course-popup pp-add-section zoom-anim-dialog mfp-hide" style="padding:100px;">
                                            <form action="JoinClassByCode" method="post">
                                                <div>
                                                    <p style="margin-top: 5px;font-size: 20px; padding: 5px 10px; height: 50px;" type="text" name="classcode" id="classcode">
                                                        You do not have permission to add people to this Class.
                                                    </p>
                                                </div>
                                                <div class="pp-footer">
                                                    <a href="#" style="margin-top: 10px;padding: 15px 10px 18px 10px;font-size: 40px; height: 60px;" class="cancel mc-btn-3 btn-style-5">Cancel</a>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <!-- Modal content-->
                            <c:if test="${isAdmin==true}">
                                <div class="meta-categories">
                                    <div class="add-section">
                                        <p><a href="#addMember" class="link mc-btn-3 btn-style-1 popup-with-zoom-anim" ><i class="icon md-user-plus"></i><span> Add Member</span></a></p>
                                        <div class="modal-content design-course-popup pp-add-section zoom-anim-dialog mfp-hide" id="addMember" >
                                            <div class="modal-header" style="padding:25px 40px;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4><span class="glyphicon glyphicon-user"></span> <b>Add People</b></h4>
                                            </div>
                                            <div class="modal-body" style="padding:40px 50px;">
                                                <form action="AddPeopleToClassController" method="POST" >
                                                    <input name="classid" value="${classid}" hidden="">
                                                    <div class="form-group">
                                                        <h2>To add members to this class, enter their username or email below (separate the members you want to add with a ",").</h2>
                                                        <input type="text" class="form-control" id="psw" style="margin-top: 27px; padding: 20px 25px; border-radius: 7px;" placeholder="Enter Email" name="email">
                                                    </div>
                                                    <button type="submit" class=" addM btn btn-success btn-block" style="" ><span class="glyphicon glyphicon-plus-sign"></span> Add</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </c:if>
                            <div class="meta-categories" >
                                <c:if test="${isAdmin==true||classOfId.isAllowMemberCreateQuiz()==true}">
                                    <p><a href="CreateQuizController?classid=${classid}" class="link" ><i class="glyphicon glyphicon-plus"></i><span> Create Quiz</span></a></p>
                                            </c:if>
                                            <c:if test="${isAdmin==false && classOfId.isAllowMemberCreateQuiz()==false}">
                                    <p><a href="#createQ" class="link mc-btn-3 btn-style-1 popup-with-zoom-anim" ><i class="glyphicon glyphicon-plus"></i><span> Create Quiz</span></a></p>
                                            </c:if>

                            </div>
                            <div class=" createQu modal-content design-course-popup pp-add-section zoom-anim-dialog mfp-hide" id="createQ" >
                                <div class="modal-header1" style="padding:35px 50px; background-color: white; color: red">
                                    <h5 style="color: red; font-size: 50px;"><span class="glyphicon glyphicon-info-sign"></span> error!</h5>
                                </div>
                                <div class="modal-body" style="padding:20px 50px; margin-bottom: 50px">
                                    <h1>You do not have permission to create Quiz in this Class.</h1>
                                </div>
                            </div>
                            <div class="meta-categories">
                                <p><a href="ViewTestResultsInClassController?classid=${classid}" class="link"><i class="icon md-shopping"></i><span> Test Results</span></a></p>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <div style="margin-top: 26px" class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog" style="text-align: center">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="padding:35px 50px;">
                            <h4 class="notice" style="color: red; font-weight: 600"><span class=""></span> ! Notice from system</h4>
                        </div>
                        <div class="modal-body" style="padding:40px 50px;">
                            <p style="font-size:22px; font-weight: 600">Hi ${sessionScope.user.getUserName()}, you can not rate your Quiz!</p>
                        </div>
                        <div class="modal-footer">
                            <div style="float:right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- FOOTER -->
            <jsp:include page="Footer.jsp"></jsp:include>
            <!-- END / FOOTER -->

        </div>
        <!-- END / PAGE WRAP -->
        <script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>
        <script src="jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="js/library/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/library/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/library/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="js/library/jquery.easing.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" src="js/library/jquery.magnific-popup.min.js"></script>

        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
        <!-- Latest compiled JavaScript -->
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>-->
        <script>
                                                                if ($('.popup-with-zoom-anim').length) {
                                                                    $('.popup-with-zoom-anim').magnificPopup({
                                                                        type: 'inline',

                                                                        fixedContentPos: false,
                                                                        fixedBgPos: true,

                                                                        overflowY: 'auto',

                                                                        closeBtnInside: true,
                                                                        preloader: false,

                                                                        midClick: true,
                                                                        removalDelay: 300,
                                                                        mainClass: 'my-mfp-zoom-in'
                                                                    });
                                                                    $('.design-course-popup').delegate('.cancel', 'click', function (evt) {
                                                                        evt.preventDefault();
                                                                        $('.mfp-close').trigger('click');
                                                                    });
                                                                }
        </script>
    </body>

</html>
